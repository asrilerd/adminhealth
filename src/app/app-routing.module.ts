import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './dashboard/components/home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guard/auth.guard';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TestTableComponent } from './test-table/test-table.component';
import { TestGraphComponent } from './test-graph/test-graph.component';
import { TestDetailComponent } from './test-detail/test-detail.component';
import { TestUserdetailComponent } from './test-userdetail/test-userdetail.component';
import { DoctorprofileComponent } from './doctorprofile/doctorprofile.component';
import { UploadComponent } from './upload/upload.component';
import { ChatComponent } from './chat/chat.component';
import { SinglepageComponent } from './singlepage/singlepage.component';
import * as firebase from 'firebase';
import { DoctorDashboardComponent } from './doctor-dashboard/doctor-dashboard.component';
// import { AuthGuard } from './shared/guard/auth.guard';
const myUser = JSON.parse(localStorage.getItem('user'));
const routes: Routes = [
  {
    path: 'setting',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
  },
  { path: 'home', component: HomeComponent },
  { path: 'Doctorhome', component: DoctorDashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registor', component: SignUpComponent },
  { path: 'testReport', component: TestTableComponent },
  {
    path: 'testDetailGraph/:id/:start/:end',
    component: TestUserdetailComponent,
  },
  { path: 'testuserlist/:id/:start/:end', component: TestGraphComponent },
  { path: 'testDetailinfo/:id', component: TestDetailComponent },
  { path: 'profile/:id', component: DoctorprofileComponent },
  { path: 'upload', component: UploadComponent },
  { path: 'chat', component: ChatComponent },
  { path: '', component: SinglepageComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule],
})
export class AppRoutingModule {}
