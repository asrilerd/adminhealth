import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavhornzComponent } from './navhornz.component';

describe('NavhornzComponent', () => {
  let component: NavhornzComponent;
  let fixture: ComponentFixture<NavhornzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavhornzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavhornzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
