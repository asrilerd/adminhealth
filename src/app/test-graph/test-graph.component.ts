import { Component, OnInit, ViewChild } from '@angular/core';
import { DoctorService } from '../service/doctor.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';
import { SSL_OP_NO_TLSv1_1 } from 'constants';
import { MatTableDataSource } from '@angular/material/table';
import { TestList } from '../models/map.model';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import { MatPaginator } from '@angular/material/paginator';
@Component({
  selector: 'app-test-graph',
  templateUrl: './test-graph.component.html',
  styleUrls: ['./test-graph.component.css'],
})
export class TestGraphComponent implements OnInit {
  constructor(
    private firebase: DoctorService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}
  get pageIndex() {
    return this.paginator.pageIndex * this.paginator.pageSize + 1;
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  testList;
  lineChart = [];
  result = [];
  dataLenght;
  dataSource;
  typeChart: any;
  dataChart: any;
  optionsChart: any;
  title;
  type;
  data;
  columnNames;
  options;
  testName;
  showdate;
  timeString = [];
  timeGraphArr = [];
  dataforgraph_genderX = [];
  dataforgraph_genderY = [];
  user;
  gender = [];
  pageSize: number[] = [5, 10, 25, 30];
  totalTable;
  displayedColumns: string[] = [
    'no',
    'user',
    'gender',
    'date',
    'score',
    'result',
  ];
  ngOnInit(): void {
    // this.depressionGraph();
    // this.LineChartMood();
    // this.googleChart();
    // this.circleChartMood();
    // this.Testdetail();
    this.result = [];
    const testName = this.route.snapshot.paramMap.get('id');
    const startstmp = this.route.snapshot.paramMap.get('start');
    const endstmp = this.route.snapshot.paramMap.get('end');
    const x = parseInt(startstmp);
    const y = parseInt(endstmp);
    const startInt = new Date(x);
    const endInt = new Date(y);
    this.testName = testName;
    this.showdate =
      startInt.toLocaleDateString() + ' - ' + endInt.toLocaleDateString();
    if (testName === 'แบบทดสอบโรคซึมเศร้า9Q') {
      this.searchDepression();
    } else if (testName === 'แบบทดสอบโรคเครียด') {
      this.searchStress();
    } else if (testName === 'แบบทดสอบตัวชี้วัดความสุข15ข้อ') {
      this.searchHappiness();
    } else if (testName === 'แบบทดสอบการฆ่าตัวตาย8Q') {
      this.searchSuicide();
    }
    const startstr = new Date(parseInt(startstmp)).getDate();
    const endstr = new Date(parseInt(endstmp)).getDate();
    const i = endstr - startstr;
    for (let x = startstr; x <= endstr; x++) {
      let a = new Date(x)
      this.timeGraphArr.push(x);
    }
    console.log( this.timeGraphArr);

  }

  showPage(page) {
    console.log(page);
    const pageSize = page.pageSize;
    const pageIndex = page.pageIndex;
    const pagePrev = page.previousPageIndex;
    // this.Testdetail();
  }

  detailUser(username) {
    console.log(username);
    this.router.navigate([`testDetailinfo/${username}`]);
  }


  searchDepression() {
    this.result = [];
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Depression().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) => {
            this.result.push(item.val());
            let timeDatestr = new Date(
              item.val().completeDT
            ).toLocaleDateString();
            this.timeString.push(timeDatestr);
            console.log(this.timeString);
            this.dataSource = new MatTableDataSource<any>(this.result);
            this.dataSource.paginator = this.paginator;
            this.dataLenght = this.result.length;
          });
      });
    });
  }

  // depressionGraph() {
  //   this.firebase.getTestList_Depression().subscribe((data) => {
  //     data.map((e) => {
  //       e.payload.child('gender').ref.on('value', (snap, prev) => {
  //         if (snap.val() === 'หญิง') {
  //           this.dataforgraph_genderX.push(snap.val());
  //         } else if (snap.val() === 'ชาย') {
  //           this.dataforgraph_genderY.push(snap.val());
  //         }
  //         this.gender.push(snap.val());
  //         console.log(this.gender);

  //       });
  //     });
  //   });
  // }

  searchHappiness() {
    this.result = [];
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Happy().subscribe((data) => {
      data.map((e) => {
        e.payload
        .child('result')
        .ref.orderByChild('completeDT')
        .startAt(parseInt(startAt))
        .endAt(parseInt(endAt))
        .on('child_added', (item, prev) => {
          this.result.push(item.val());
          let timeDatestr = new Date(
            item.val().completeDT
          ).toLocaleDateString();
          this.timeString.push(timeDatestr);
          console.log(this.result);
          this.dataSource = new MatTableDataSource<any>(this.result);
          this.dataSource.paginator = this.paginator;
          this.dataLenght = this.result.length;
        });
      });
    });
  }

  searchStress() {
    this.result = [];
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Stress().subscribe((data) => {
      data.map((e) => {
        e.payload
        .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) =>{
          this.result.push(item.val());
          let timeDatestr = new Date(
            item.val().completeDT
          ).toLocaleDateString();
          this.timeString.push(timeDatestr);
          this.dataSource = new MatTableDataSource<any>(this.result);
          this.dataSource.paginator = this.paginator;
          this.dataLenght = this.result.length;
        });
      });
    });
  }
  searchSuicide() {
    this.result = [];
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Suicide().subscribe((data) => {
      data.map((e) => {
        e.payload
        .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) => {
          this.result.push(item.val());
          let timeDatestr = new Date(
            item.val().completeDT
          ).toLocaleDateString();
          this.timeString.push(timeDatestr);
          this.dataSource = new MatTableDataSource<any>(this.result);
          this.dataSource.paginator = this.paginator;
          this.dataLenght = this.result.length;
        });
      });
    });
  }

  //google

  googleChart() {
    this.title = '';
    this.type = 'ColumnChart';
    this.data = [
      ['female ', this.dataforgraph_genderX.length, '#b87333'],
      ['male ', this.dataforgraph_genderY.length, '#50c0c0'],
    ];
    this.columnNames = ['', '', { role: 'style', type: 'string' }];
    this.options = {
      bar: { groupWidth: '40%' },
      legend: { position: 'none' },
    };
  }

  downloadPdf(testname,date) {
    const table = document.getElementById('table');
    html2canvas(table).then((canvas) => {
      console.log(canvas);
      const imgData = canvas.toDataURL('image/png');
      const imgWeight = 180;
      const imgHeight = (canvas.height * imgWeight) / canvas.width;
      const doc = new jsPDF();
      doc.text(`${date}`, 15, 15);
      doc.addImage(imgData, 15, 20, imgWeight, imgHeight);
      doc.save(`ตารางรายการบุคคล${testname}.pdf`);
    });
  }
  ///line///

  LineChartMood() {
    // console.log('line');

    this.lineChart = new Chart('lineChart', {
      type: 'bar',
      data: {
        // yLabels: ["Python", "Octave/MATLAB", "JavaScript"],
        // xLabels: ["Beginner", "Intermediate", "Advanced"],
        labels: ['male', 'female'], //แกน x
        datasets: [
          {
            label: '',
            data: [1, 2],
            fill: false,
            lineTension: 0.2,
            borderWidth: 1,
            borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: 'rgba(75, 192, 192, 1)',
            steppedLine: false,
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'FontAwesome',
                fontSize: 20,
                callback: function (value, index, values) {
                  if (index === 0) {
                    return `${value}  \uf556`;
                  } else if (index === 1) {
                    return '\uf5b4';
                  }
                  // } else if (index === 2) {
                  //   return "\uf5a4";
                  // } else if (index === 3) {
                  //   return "\uf57a";
                  // } else if (index === 4) {
                  //   return "\uf5a5";
                  // } else if (index === 5) {
                  //   return "\uf5c8";
                  // } else if (index === 6) {
                  //   return "\uf598";
                  // } else if (index === 7) {
                  //   return "\uf58c";
                  // }
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }
  circleChartMood() {
    this.lineChart = new Chart('circleChart', {
      type: 'doughnut',
      data: {
        labels: [
          'angry',
          'sad',
          'lonely',
          'feeling blue',
          'bored',
          'tired',
          'chill',
          'happy',
        ], //แกน x
        datasets: [
          {
            label: '',
            data: [1, 1, 1, 1, 1, 1, 1, 1, 1],
            fill: true,
            lineTension: 0.2,
            // borderColor: "rgba(75, 192, 192, 1)",
            backgroundColor: [
              'pink',
              'gold',
              'rgba(75, 192, 192, 1)',
              'lightblue',
              'rgba(75, 192, 192)',
              'red',
              'violet',
              'green',
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        legend: {
          display: false,
        },
        tooltip: {
          enabled: false,
        },
        cutoutPercentage: 60,
      },
      scale: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    });
  }
}
