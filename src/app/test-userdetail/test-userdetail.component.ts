import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';
import { DoctorService } from '../service/doctor.service';
import { log } from 'util';
@Component({
  selector: 'app-test-userdetail',
  templateUrl: './test-userdetail.component.html',
  styleUrls: ['./test-userdetail.component.css'],
})
export class TestUserdetailComponent implements OnInit {
  lineChart = [];
  pageSize: number[] = [5, 10, 25, 30];
  totalTable;
  result = [];
  result_x = [];
  result_y = [];
  scoreX = 0;
  scoreY = 0;
  score = 0;
  dataLenght;
  dataSource;
  timeString = [];
  timeGraphArr = [];
  showdate;
  testName;
  lenghtX = 0;
  lenghtY = 0;
  displayedColumns: string[] = [
    'amout',
    'amoutX',
    'amoutY',
    'scoreX',
    'scoreY',
    'score',
  ];
  sx;
  sy;

  get pageIndex() {
    return this.paginator.pageIndex * this.paginator.pageSize + 1;
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private firebase: DoctorService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  userDatail() {
    console.log('Go!!!');
    const startstmp = this.route.snapshot.paramMap.get('start');
    const endstmp = this.route.snapshot.paramMap.get('end');
    this.router.navigate([
      `testuserlist/${this.testName}/${startstmp}/${endstmp}`,
    ]);
  }
  ngOnInit(): void {
    this.Testdetail();
    const startstmp = this.route.snapshot.paramMap.get('start');
    const endstmp = this.route.snapshot.paramMap.get('end');
    const startstr = new Date(parseInt(startstmp)).toLocaleDateString();
    const endstr = new Date(parseInt(endstmp)).toLocaleDateString();
    console.log(startstr, endstr);
    // const date1: any = new Date(startstmp);
    // const date2: any = new Date(endstmp);
    // const diffTime = Math.abs(date2 - date1);
    // const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    // console.log(diffTime + ' milliseconds');
    // console.log(diffDays + ' days');
  }

  Testdetail() {
    this.result = [];
    const testName = this.route.snapshot.paramMap.get('id');
    const startstmp = this.route.snapshot.paramMap.get('start');
    const endstmp = this.route.snapshot.paramMap.get('end');
    const x = parseInt(startstmp);
    const y = parseInt(endstmp);
    const startInt = new Date(x);
    const endInt = new Date(y);
    this.testName = testName;
    this.showdate =
      startInt.toLocaleDateString() + ' - ' + endInt.toLocaleDateString();
    if (testName === 'แบบทดสอบโรคซึมเศร้า9Q') {
      this.searchDepression();
    } else if (testName === 'แบบทดสอบโรคเครียด') {
      this.searchStress();
    } else if (testName === 'แบบทดสอบตัวชี้วัดความสุข15ข้อ') {
      this.searchHappiness();
    } else if (testName === 'แบบทดสอบการฆ่าตัวตาย8Q') {
      this.searchSuicide();
    }
  }

  //*************************/ DEPRESSION TEST *********************************//
  searchDepression() {
    this.result = [];
    const testName = this.route.snapshot.paramMap.get('id');
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Depression().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) => {
            this.result.push(item.val());
            let timeDatestr = new Date(
              item.val().completeDT
            ).toLocaleDateString();
            this.timeString.push(timeDatestr);
            this.score = this.score + item.val().totalScore;
          });
        const genderY = this.result.filter((member) => {
          return member.gender == 'ชาย';
        });
        const genderX = this.result.filter((member) => {
          return member.gender == 'หญิง';
        });
        this.lenghtX = genderX.length;
        this.lenghtY = genderY.length;

        ////คำนวนกราฟ โยนคะแนนพล็อตกราฟ///
        this.result_x.push(genderX);
        this.result_y.push(genderY);
        this.sx = genderX.map((i) => i.totalScore);
        this.sy = genderY.map((i) => i.totalScore);
        this.LineChart();
        this.barChart();
      });
      setTimeout(() => {
        for (let i = 0; i < this.sy.length; i++) {
          this.scoreY = this.scoreY + this.sy[i];
          console.log(this.sy[i]);
        }
        for (let i = 0; i < this.sx.length; i++) {
          this.scoreX = this.scoreX + this.sx[i];
          console.log(this.sx[i]);
        }
        this.LineChart();
      }, 0);
    });
  }
  //***************************** STRESS TEST *******************************//
  searchStress() {
    this.result = [];
    const testName = this.route.snapshot.paramMap.get('id');
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Stress().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) => {
            this.result.push(item.val());
            let timeDatestr = new Date(
              item.val().completeDT
            ).toLocaleDateString();
            this.timeString.push(timeDatestr);
            this.dataSource = new MatTableDataSource<any>(this.result);
            this.dataLenght = this.result.length;
            this.score = this.score + item.val().totalScore;
          });
        const genderY = this.result.filter((member) => {
          return member.gender == 'ชาย';
        });
        const genderX = this.result.filter((member) => {
          return member.gender == 'หญิง';
        });
        this.lenghtX = genderX.length;
        this.lenghtY = genderY.length;
        ////คำนวนกราฟ โยนคะแนนพล็อตกราฟ///
        this.result_x.push(genderX);
        this.result_y.push(genderY);
        this.sx = genderX.map((i) => i.totalScore);
        this.sy = genderY.map((i) => i.totalScore);
        console.log('filter arrY', genderY);
        console.log('filter arrX', genderX);
        console.log(
          'filter dayX',
          genderX.map((i) => new Date(i.completeDT).toLocaleDateString())
        );
        console.log(
          'filter dayY',
          genderY.map((i) => new Date(i.completeDT).toLocaleDateString())
        );
        this.LineChart();
        this.barChart();
      });
      setTimeout(() => {
        for (let i = 0; i < this.sy.length; i++) {
          this.scoreY = this.scoreY + this.sy[i];
          console.log(this.sy[i]);
        }
        for (let i = 0; i < this.sx.length; i++) {
          this.scoreX = this.scoreX + this.sx[i];
          console.log(this.sx[i]);
        }
        this.LineChart();
      }, 0);
    });
  }
  //***************************** SUICIDE TEST ******************************//
  searchHappiness() {
    this.result = [];
    const testName = this.route.snapshot.paramMap.get('id');
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Happy().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) => {
            this.result.push(item.val());
            let timeDatestr = new Date(
              item.val().completeDT
            ).toLocaleDateString();
            this.timeString.push(timeDatestr);
            this.dataSource = new MatTableDataSource<any>(this.result);
            this.dataLenght = this.result.length;
            this.score = this.score + item.val().totalScore;
          });
        const genderY = this.result.filter((member) => {
          return member.gender == 'ชาย';
        });
        const genderX = this.result.filter((member) => {
          return member.gender == 'หญิง';
        });
        this.lenghtX = genderX.length;
        this.lenghtY = genderY.length;
        ////คำนวนกราฟ โยนคะแนนพล็อตกราฟ///
        this.result_x.push(genderX);
        this.result_y.push(genderY);
        this.sx = genderX.map((i) => i.totalScore);
        this.sy = genderY.map((i) => i.totalScore);
        console.log('filter arrY', genderY);
        console.log('filter arrX', genderX);
        console.log(
          'filter dayX',
          genderX.map((i) => new Date(i.completeDT).toLocaleDateString())
        );
        console.log(
          'filter dayY',
          genderY.map((i) => new Date(i.completeDT).toLocaleDateString())
        );

        this.LineChart();
        this.barChart();
      });
      setTimeout(() => {
        for (let i = 0; i < this.sy.length; i++) {
          this.scoreY = this.scoreY + this.sy[i];
          console.log(this.sy[i]);
        }
        for (let i = 0; i < this.sx.length; i++) {
          this.scoreX = this.scoreX + this.sx[i];
          console.log(this.sx[i]);
        }
        this.LineChart();
      }, 0);
    });
  }
  //***************************** HAPPY TEST ******************************* //
  searchSuicide() {
    this.result = [];
    const testName = this.route.snapshot.paramMap.get('id');
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    this.firebase.getTestList_Suicide().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(parseInt(startAt))
          .endAt(parseInt(endAt))
          .on('child_added', (item, prev) => {
            this.result.push(item.val());
            let timeDatestr = new Date(
              item.val().completeDT
            ).toLocaleDateString();
            this.timeString.push(timeDatestr);
            this.dataSource = new MatTableDataSource<any>(this.result);
            this.dataLenght = this.result.length;
            this.score = this.score + item.val().totalScore;
          });
        const genderY = this.result.filter((member) => {
          return member.gender == 'ชาย';
        });
        const genderX = this.result.filter((member) => {
          return member.gender == 'หญิง';
        });
        this.lenghtX = genderX.length;
        this.lenghtY = genderY.length;
        ////คำนวนกราฟ โยนคะแนนพล็อตกราฟ///
        this.result_x.push(genderX);
        this.result_y.push(genderY);
        this.sx = genderX.map((i) => i.totalScore);
        this.sy = genderY.map((i) => i.totalScore);
        console.log('filter arrY', genderY);
        console.log('filter arrX', genderX);
        console.log(
          'filter dayX',
          genderX.map((i) => new Date(i.completeDT).toLocaleDateString())
        );
        console.log(
          'filter dayY',
          genderY.map((i) => new Date(i.completeDT).toLocaleDateString())
        );

        this.LineChart();
        this.barChart();
      });
      setTimeout(() => {
        for (let i = 0; i < this.sy.length; i++) {
          this.scoreY = this.scoreY + this.sy[i];
          console.log(this.sy[i]);
        }
        for (let i = 0; i < this.sx.length; i++) {
          this.scoreX = this.scoreX + this.sx[i];
          console.log(this.sx[i]);
        }
        this.LineChart();
      }, 0);

    });
  }

  //**********************************depressX*****************************************/

  //**********************************stressX*****************************************/

  //**********************************happyX*****************************************/

  //**********************************suicideX วิธีเก่า กรณีศึกษา*****************************************/
  suicideX() {
    // const testName = this.route.snapshot.paramMap.get('id');
    // const startAt = this.route.snapshot.paramMap.get('start');
    // const endAt = this.route.snapshot.paramMap.get('end');
    // this.firebase.getTestList_Suicide().subscribe((data) => {
    //   data.map((e) => {
    //     e.payload
    //       .child('result')
    //       .ref.orderByChild('gender')
    //       .equalTo("หญิง")
    //       .on('value', (snap, prev) => {
    //         if (snap.val()) {
    //           console.log(snap.val());
    //           snap.ref
    //             .orderByChild('completeDT')
    //             .startAt(parseInt(startAt))
    //             .endAt(parseInt(endAt))
    //             .on('child_added', (i, prev) => {
    //               console.log('หญิง', i.val());
    //               this.result_x.push(i.val());
    //               this.scoreX = this.scoreX + i.val().totalScore;
    //             });
    //           this.LineChart();
    //           this.barChart();
    //         } else {
    //           console.log('no data');
    //         }
    //       });
    // e.payload
    //   .child('result')
    //   .ref.orderByChild('gender')
    //   .equalTo('ชาย')
    //   .on('value', (snap, prev) => {
    //     if (snap.val()) {
    //       console.log(snap.val());
    //       snap.ref
    //         .orderByChild('completeDT')
    //         .startAt(parseInt(startAt))
    //         .endAt(parseInt(endAt))
    //         .on('child_added', (i, prev) => {
    //           console.log('ชาย', i.val());
    //           this.result_y.push(i.val());
    //           this.scoreY = this.scoreY + i.val().totalScore;
    //         });
    //       this.LineChart();
    //       this.barChart();
    //     } else {
    //       console.log('no data');
    //     }
    //   });
    //  });
    // });
  }

  /******************* bar chart ***************************/
  barChart() {
    const startAt = this.route.snapshot.paramMap.get('start');
    const endAt = this.route.snapshot.paramMap.get('end');
    const start = parseInt(startAt);
    const end = parseInt(endAt);
    const differentDate = Math.round(
      Math.abs((start - end) / (24 * 60 * 60 * 1000))
    );
    // console.log('differentDate : ', differentDate);
    // นำระยะห่างของวันที่เลือกมาคำนวนหาวันที่ ระหว่าง วันเริ่ม กับ วันจบ // Array.from( object , map.function , value )
    const days = Array.from(new Array(differentDate + 1), (v, i) =>
      new Date(start + i * (24 * 60 * 60 * 1000)).toLocaleDateString()
    );
    const dataarrX = [];
    const dataarrY = [];
    let dataarrTotal = [];
    const genderY = this.result.filter((member) => {
      return member.gender == 'ชาย';
    });
    const genderX = this.result.filter((member) => {
      return member.gender == 'หญิง';
    });
    /*********** result data female ************************/
    for (let x = 0; x < differentDate + 1; x++) {
      const dateArr = genderX
        .map((e) => new Date(e.completeDT).toLocaleDateString())
        .filter((result) => {
          return result === days[x];
          // console.log(result);
        });
      if (dateArr.length) {
        dataarrX.push(dateArr.length);
        // console.log(dataarrX);
      } else if (dateArr.length == 0) {
        dataarrX.push(dateArr.length);
      }
    }
    /*********** result data male ************************/
    for (let x = 0; x < differentDate + 1; x++) {
      const dateArr = genderY
        .map((e) => new Date(e.completeDT).toLocaleDateString())
        .filter((result) => {
          return result === days[x];
        });
      if (dateArr.length) {
        dataarrY.push(dateArr.length);
        // console.log(dataarrY);
      } else if (dateArr.length == 0) {
        dataarrY.push(dateArr.length);
      }
    }
    for (let x = 0; x < differentDate + 1; x++) {
      dataarrTotal.push(dataarrX[x] + dataarrY[x]);
      // console.log(dataarrTotal);
    }

    this.lineChart = new Chart('BarChart', {
      type: 'line',
      data: {
        labels: days, //แกน x
        datasets: [
          //หญิง
          {
            label: '',
            data: dataarrX,
            fill: false,
            lineTension: 0,
            borderWidth: 2,
            borderColor: 'hotpink',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            steppedLine: false,
          },

          //ชาย
          {
            label: '',
            data: dataarrY,
            fill: false,
            lineTension: 0,
            borderWidth: 2,
            borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            steppedLine: false,
          },
          //รวม
          {
            label: '',
            data: dataarrTotal,
            fill: false,
            lineTension: 0,
            borderWidth: 2,
            borderColor: 'orange',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            steppedLine: false,
          },
        ],
        pointStyle: 'triangle',
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'cloud',
                fontSize: 10,
                callback: function (value, index, values) {
                  return value;
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }
  r(r: any) {
    throw new Error('Method not implemented.');
  }

  /******************* bar chart ***************************/
  LineChart() {
    // console.log('line');
    const x = (this.scoreX / this.lenghtX).toFixed(2);
    const y = (this.scoreY / this.lenghtY).toFixed(2);
    const i = (this.score / this.result.length).toFixed(2);
    this.lineChart = new Chart('lineChart', {
      type: 'bar',
      data: {
        // yLabels: ["Python", "Octave/MATLAB", "JavaScript"],
        // xLabels: ["Beginner", "Intermediate", "Advanced"],
        labels: ['female', 'male', 'total'], //แกน x
        datasets: [
          {
            label: '',
            data: [x, y, i],
            fill: false,
            lineTension: 0.2,
            borderWidth: 1,
            // borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: ['Hotpink', '#4bc0c0', 'orange'],
            steppedLine: false,
          },
          //   {
          //     label: 'Line Dataset',
          //     data: [i],
          //     borderColor: 'black',
          //     fill:false,
          //     // Changes this dataset to become a line
          //     type: 'line'
          // }
        ],
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'cloud',
                fontSize: 14,
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }
}
