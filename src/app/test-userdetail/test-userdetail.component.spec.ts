import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestUserdetailComponent } from './test-userdetail.component';

describe('TestUserdetailComponent', () => {
  let component: TestUserdetailComponent;
  let fixture: ComponentFixture<TestUserdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestUserdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestUserdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
