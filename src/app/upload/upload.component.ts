import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
// import { AngularFireStorage } from 'angularfire2/storage';
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil, filter, switchMap } from 'rxjs/operators';
// import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// import { FirebaseService } from '../../services/firebase.service';
import { async, inject } from '@angular/core/testing';
import { element } from 'protractor';
import * as firebase from 'firebase';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppSettings } from 'src/app/app-settings';
import { UploadEditComponent } from 'src/app/dialog/upload-edit/upload-edit.component';
import { DOCUMENT } from '@angular/common';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { FirebaseService } from '../dashboard/services/firebase.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
// import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit ,OnDestroy {

  private unsubscribe$ = new Subject();
  title = "cloudsSorage";
  selectedFile: File = null;
  fb;
  totalTable;
  downloadURL: Observable<string>;
  uploadedList;
  showDocument= 'none';
  datasource;
  displayedColumns: string[] = [
    'name',
    'date',
    'fequency',
    'action'
  ];
  pageSize: number[] = [5, 10, 25, 30] ;
  pageSizeOption:number = 10;
  get pageIndex () {
    return (this.paginator.pageIndex  *  this.paginator.pageSize )+1 ;
   }
   @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private storage : AngularFireStorage,
    private firebase: FirebaseService,
    private db: AngularFireDatabase,
    private dialog: MatDialog,

    @Inject(DOCUMENT)  private document: Document
  ) { }

  ngOnInit(): void {
    this.getFileUploaded();
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  showPage(page){
    console.log(page);
    const pageSize = page.pageSize ;
    const pageIndex = page.pageIndex ;
    const pagePrev  = page.previousPageIndex ;
     this.getFileUploaded();
  }

  onFileSelected(event){
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `document/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`document/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize( async () => {
          this.downloadURL = await fileRef.getDownloadURL().toPromise() ;
          this.db.database.ref('Document').push( {path:filePath, name: file.name , imageUrl:  this.downloadURL , size: file.size } );
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url,'hulat');
        }
      });


  }

  go(url){
    console.log(url);
    window.open(url.value.imageUrl);
  }

  getFileUploaded() {
    this.firebase.uploadList(this.pageSizeOption).subscribe(
      data => {
        this.uploadedList = data.map(
          item => {
             return {
               key : item.key,
               value: item.payload.val()
             }
          });
            console.log(this.uploadedList);
            return this.datasource = new MatTableDataSource(this.uploadedList),
            // console.log(this.tableList),
            this.datasource.paginator =  this.paginator;
      }
    )
  }
  getdata(element){
    console.log(
      `data : ${element.key} ${element.value.path}`
    );

  }

  deleteFileData(data){
    this.db.list('Document').remove(data.key) ;
    let storageRef = firebase.storage().ref(`${data.value.path}`)
    storageRef.delete();
  }


  editFile(element) {
    const dialogConfig: MatDialogConfig = {
      data: element
    };
    const dialogRef = this.dialog.open(UploadEditComponent,dialogConfig);
    dialogRef.afterClosed().pipe(
      takeUntil(this.unsubscribe$),
      filter(data => !! data),
      switchMap( data => {

        return this.firebase.updateNewFile(element.key,data);
      })

    ).subscribe( res => {
      this.getFileUploaded();

    });
    console.log('already edit');
  }

}

