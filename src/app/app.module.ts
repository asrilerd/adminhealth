import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardModule } from './dashboard/dashboard.module';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { AngularFireDatabaseModule } from 'angularfire2/database';
// import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { SignUpComponent } from './sign-up/sign-up.component';
import { TestTableComponent } from './test-table/test-table.component';
import { TestGraphComponent } from './test-graph/test-graph.component';
import { TestDetailComponent } from './test-detail/test-detail.component';
// import { AngularFireModule } from 'angularfire2';
import { GoogleChartsModule } from 'angular-google-charts';
import { TestUserdetailComponent } from './test-userdetail/test-userdetail.component';
import { MenubarDoctorComponent } from './menubar-doctor/menubar-doctor.component';
import { DoctorprofileComponent } from './doctorprofile/doctorprofile.component';
import { NavhornzComponent } from './navhornz/navhornz.component';
import { UploadComponent } from './upload/upload.component';
import { ChatComponent } from './chat/chat.component';
import { SinglepageComponent } from './singlepage/singlepage.component';
import { DoctorDashboardComponent } from './doctor-dashboard/doctor-dashboard.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    TestTableComponent,
    TestGraphComponent,
    TestDetailComponent,
    TestUserdetailComponent,
    MenubarDoctorComponent,
    DoctorprofileComponent,
    UploadComponent,
    ChatComponent,
    SinglepageComponent,
    DoctorDashboardComponent,
    // NavhornzComponent
  ],
  imports: [
    BrowserModule,
    AngularMaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DashboardModule,
    ReactiveFormsModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC0zGOYK2t0CDSi1c5nfw0WPrUsM6l1VUU'
    }),
    HttpClientModule,
    GoogleChartsModule
  ],
  entryComponents:[LoginComponent],
  providers: [AngularFireDatabaseModule,AngularFireAuthModule,AngularFireModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
