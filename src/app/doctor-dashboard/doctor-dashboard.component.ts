import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';
import { AdminEditProfileComponent } from '../dashboard/components/admin-edit-profile/admin-edit-profile.component';
import { EditPasswordComponent } from '../dialog/edit-password/edit-password.component';

@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.css'],
})
export class DoctorDashboardComponent implements OnInit {
  date = new Date().toDateString();
  userEdit;
  myusername;
  key;
  display;
  dp = 0;
  ss = 0;
  st = 0;
  hp = 0;
  totalPeoples = 0;
  myUser = JSON.parse(localStorage.getItem('user')).user.uid;
  xDp = [];
  xSs = [];
  xSt = [];
  xHp = [];
  gender = [];
  keies = [];
  showX;
  showY;
  unsubscribe$ = new Subject();
  localuser = JSON.parse(localStorage.getItem('user'));
  createAt = this.localuser.user.createdAt;
  lastlogin = this.localuser.user.lastLoginAt;
  creatAtday = new Date(parseInt(this.createAt)).toLocaleDateString();
  lastloginDate = new Date(parseInt(this.lastlogin)).toLocaleDateString();

  constructor(private firebase: FirebaseService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.getuserbyID();
    this.getScore();
  }

  getuserbyID() {
    this.firebase.getUserbyUID(this.myUser).subscribe((username) => {
      this.userEdit = username.map((item) => {
        this.myusername = item.payload.val();
        this.key = item.key;
        return {
          key: item.key,
          value: item.payload.val(),
        };
      });
      if (this.myusername.gender == 'หญิง') {
        this.display = './assets/businuesswoman23.png';
      } else {
        this.display = './assets/businessman.png';
      }
    });
  }

  getScore() {
    this.firebase.getdepressScore().subscribe((data) => {
      this.dp = data.length;
      data.map((item) => {
        this.keies.push(item.key);
      });
      for (let i = 0; i < this.keies.length; i++) {
        console.log(i);

        this.firebase.getUserbyUID(this.keies[i]).subscribe((username) => {
          username.map((item) => {
            this.myusername = item.payload.val();
            // console.log(this.myusername.length);
          });
          if (this.myusername.gender == 'หญิง') {
            this.xDp.push( this.myusername.gender)
            // console.log('XDp',this.xDp);
          }
        });
      }
    });

    this.firebase.gethappinessScore().subscribe((data) => {
      this.hp = data.length;
      data.map((item) => {
        this.keies.push(item.key);
      });
          for (let i = 0; i < this.keies.length; i++) {
            this.firebase.getUserbyUID(this.keies[i]).subscribe((username) => {
              username.map((item) => {
                this.myusername = item.payload.val();
              });
              if (this.myusername.gender == 'หญิง') {
                this.xHp.push(this.myusername.gender);
              }
              // console.log('xHp',this.xHp);
            });
          }
    });

    this.firebase.getsuicideScore().subscribe((data) => {
      this.ss = data.length;
      data.map((item) => {
        this.keies.push(item.key);
      });
      for (let i = 0; i < this.keies.length; i++) {
        this.firebase.getUserbyUID(this.keies[i]).subscribe((username) => {
          username.map((item) => {
            this.myusername = item.payload.val();
          });
          if (this.myusername.gender == 'หญิง') {
            this.xSs.push(this.myusername.gender);
          }
          console.log('xSs',this.xSs);
        });
      }
    });
    this.firebase.getstressScore().subscribe((data) => {
      this.st = data.length;
      data.map((item) => {
        this.keies.push(item.key);
      });
      for (let i = 0; i < this.keies.length; i++) {
        this.firebase.getUserbyUID(this.keies[i]).subscribe((username) => {
          username.map((item) => {
            this.myusername = item.payload.val();
          });
          if (this.myusername.gender == 'หญิง') {
            this.xSt.push(this.myusername.gender);
            // console.log('xSt',this.xSt);
          }

        });
      }
      this.showX = this.xDp.length + this.xHp.length + this.xSs.length + this.xSt.length;
      console.log(this.showX);
      this.totalPeoples = this.dp + this.ss + this.st + this.hp;
    });
  }

  resetPassword() {
    const dialogConfig: MatDialogConfig = {
      data: this.userEdit[0],
    };
    const dialogRef = this.dialog.open(EditPasswordComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          console.log('pass:', data);
          return this.firebase
            .updateUser(this.key, data)
            .then((i) => {
              console.log(i, 'updataResult');
            })
            .catch((err) => {
              console.log(err);
            });
        })
      )
      .subscribe(() => {
        this.getuserbyID();
      });
  }
  editProfile() {
    const dialogConfig: MatDialogConfig = {
      data: this.userEdit[0],
    };
    const dialogRef = this.dialog.open(AdminEditProfileComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          console.log('profile:', data);
          return this.firebase
            .updateUser(this.key, data)
            .then((i) => {
              console.log(i, 'updataResult');
            })
            .catch((err) => {
              console.log(err);
            });
        })
      )
      .subscribe(() => {
        this.getuserbyID();
      });
  }
}
