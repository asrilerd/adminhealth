import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-news-edit',
  templateUrl: './news-edit.component.html',
  styleUrls: ['./news-edit.component.css']
})
export class NewsEditComponent implements OnInit {
  form: FormGroup ;
  textToolStyle = {
    height:"250px"
  }
  article
  toolconfig={
    toolbar:[
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      ['link', 'image']
    ]
  }
  constructor(
    private fb: FormBuilder,
    private dialogRef : MatDialogRef<NewsEditComponent,any>,
    @Inject(MAT_DIALOG_DATA) public data :any
  ) { }

  ngOnInit(): void {
    this.initForm(this.data);
    console.log(this.data);

  }

  private initForm(emp) {
    this.form = this.fb.group({
      topic: null ,
      article: null,
    });
    this.form.patchValue(emp)
  }

  update() {
    this.dialogRef.close(this.form.value)
  }
  maxlenght(event){
    if(event.editor.getLength() > 1000){
        event.editor.deleteText(1000,event.editor.getLength())
    }
    this.article =event.editor.getLength()
  }

}
