import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/auth.service';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';

@Component({
  selector: 'app-addnews',
  templateUrl: './addnews.component.html',
  styleUrls: ['./addnews.component.css']
})
export class AddnewsComponent implements OnInit {
  form: FormGroup;
  currentDay = new Date();
  day = this.currentDay.toDateString();
  newsType:string[] = ['ข่าวทั่วไป','ประกาศสำคัญ']
  textToolStyle = {
    height:"250px"
  }
  article
  toolconfig={
    toolbar:[
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      ['link', 'image']
    ]
  }
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    public dialogRef: MatDialogRef<AddnewsComponent>
  ) { }

  ngOnInit(): void {
    this.initForm();
  }
  private initForm() {
    this.form = this.fb.group({
      topic: [null, [Validators.required]],
      article: [null, [Validators.required]],
      createDT: [this.day, [Validators.required]],
      type:[null]
    });
  }
  newsUpload() {
    if(this.form.invalid){
      alert('กรุณากรอกข้อมูลให้ครบถ้วย')
    }else{
      this.firebase.addNews(this.form);
    }
    console.log('complete Upload!!!');
    this.form.reset();
    this.dialogRef.close();
  }
  maxlenght(event){
    if(event.editor.getLength() > 1000){
        event.editor.deleteText(1000,event.editor.getLength())
    }
    this.article =event.editor.getLength()
  }
}
