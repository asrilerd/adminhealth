import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MapDialogComponent } from '../map-dialog/map-dialog.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { AngularFireStorage } from 'angularfire2/storage';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';
import { finalize } from 'rxjs/operators';
import { async } from '@angular/core/testing';
import { Observable } from 'rxjs';
import * as firebase from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-upload-edit',
  templateUrl: './upload-edit.component.html',
  styleUrls: ['./upload-edit.component.css']
})
export class UploadEditComponent implements OnInit {
  form: FormGroup;
  file:File;
  date = new Date();
  downloadURL: Observable<string>;
  get newUpload(){
    return this.form.get('newFile');
  }
  // get currentDate(){
  //   return this.date.toDateString;
  // }

  constructor(
    private dialogRef: MatDialogRef<UploadEditComponent, any>,
    private fb: FormBuilder,
    private storage: AngularFireStorage,
    private firebase: FirebaseService,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.initForm(this.data.value);
    console.log(this.data);
    console.log(this.data.key);

  }

  private initForm(file) {
    const currentDate = this.date.toDateString();
    this.form = this.fb.group({
      name : [null, [Validators.required]],
      newFile: [null],
      date: [currentDate],
      fequency: [null]
    });
    this.form.patchValue(file);
  }
  update() {
    this.dialogRef.close(this.form.value);
  }
  cancelUpdate() {
    this.dialogRef.close();
  }
  onFileChange(event){
      const inputFile = event.target as HTMLInputElement
      console.log(inputFile);
      if( inputFile && inputFile.files.length >0 ){

        this.file = inputFile.files.item(0)
        this.showFileName()
        var n = Date.now();
        const filePath = `document/${n}`
        const fileRef = this.storage.ref(filePath)
        const task = this.storage.upload(filePath,this.file);
        task
        .snapshotChanges()
        .pipe(
          finalize( async()=>{
            this.downloadURL = await fileRef.getDownloadURL().toPromise();
            let storageRef = firebase.storage().ref(`${this.data.value.path}`)
            storageRef.delete();
            return this.firebase.updateNewFile(this.data.key, {path:filePath , imageUrl:  this.downloadURL , size: this.file.size })
          })
        ).subscribe( url => {
           console.log(url);

        })

      }


  }

  showFileName() {
    this.newUpload.setValue(this.file)
  }

}
