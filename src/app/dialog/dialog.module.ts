import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { MapDialogComponent } from './map-dialog/map-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { MapPopupComponent } from './map-popup/map-popup.component';
import { UploadEditComponent } from './upload-edit/upload-edit.component';
import { NewsEditComponent } from './news-edit/news-edit.component';
import { EditPasswordComponent } from './edit-password/edit-password.component';
import { AddnewsComponent } from './addnews/addnews.component';
import { QuillModule } from 'ngx-quill';



@NgModule({
  declarations: [MapDialogComponent, MapPopupComponent, UploadEditComponent, NewsEditComponent, EditPasswordComponent, AddnewsComponent],
  entryComponents:[MapDialogComponent,MapPopupComponent,UploadEditComponent, NewsEditComponent , EditPasswordComponent , AddnewsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
    QuillModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCqBau3CoAgOrUUux7iwl8Doz6VhcPVk04',
    }),
  ]
})
export class DialogModule { }
