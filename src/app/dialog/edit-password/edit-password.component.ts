import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';
import { AuthService } from 'src/app/auth/auth.service';
// import { AngularFireDatabase } from 'angularfire2/database';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.css']
})
export class EditPasswordComponent implements OnInit {

  form:FormGroup
  alertStatus=true;
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private auth: AuthService,
    private afs: AngularFireDatabase ,
    private dialog : MatDialog,
    private dialogRef: MatDialogRef<EditPasswordComponent, any>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit(): void {
    this.initform(this.data.value);
  }

  private initform(userData) {
    this.form = this.fb.group({
      email: [null,Validators.required] ,
      password : [null,Validators.required]
    });
    this.form.patchValue(userData);

  }

  update(){
    if(this.form.valid){
      this.alertStatus =false;
      setTimeout( ()=>{
        this.dialogRef.close(this.form.value);
      } ,2000)
    } else{
      alert('กรุณากรอกข้อมูลให้ครบ')
    }
  }
}
