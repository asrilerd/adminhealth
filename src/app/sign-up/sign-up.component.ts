import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FirebaseService } from '../dashboard/services/firebase.service';
import { AuthService } from '../auth/auth.service';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  userform: FormGroup;
  get email() {
    return this.form.get('email');
  }
  get pass() {
    return this.form.get('password');
  }
  get emailUser() {
    return this.userform.get('email');
  }
  get passUser() {
    return this.userform.get('password');
  }
  get role() {
    return this.form.get('role');
  }
  get firstname() {
    return this.form.get('firstname');
  }
  get lastname() {
    return this.form.get('lastname');
  }
  get username() {
    return this.form.get('username');
  }
  status: string[] = ['admin', 'doctor'];
  careers: string[] = [
    'นักเรียน/นักศึกษา',
    'ข้าราชการ',
    'รัฐวิสาหกิจ',
    'เจ้าของกิจการ',
    'ลูกจ้าง/พนักงาน',
    'ว่างงาน',
  ];
  genders: string[] = ['ชาย', 'หญิง'];
  edu: string[] = [
    'มัธยมต้น',
    'มัธยมปลายหรือเทียบเท่า',
    'อนุปริญญาตรี',
    'ปริญญาตรี',
    'ปริญญาโท',
    'สูงกว่าปริญญาโท',
  ];
  statusMat: string[] = ['โสด', 'แต่งงาน', 'หย่าร้าง', 'แยกกันอยู่'];
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private auth: AuthService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.initform();
  }

  private initform() {
    this.form = this.fb.group({
      firstname: null,
      lastname: null,
      username: null,
      email: null,
      password: null,
      role: null,
    });

    this.userform = this.fb.group({
      firstname: null,
      lastname: null,
      user: null,
      email: null,
      password: null,
      role: 'user',
      gender: null,
      age: null,
      career: null,
      education: null,
      maritialStatus: null,
    });
  }
  adduser() {
    if (this.userform.valid) {
      this.auth
        .AddUser(this.emailUser.value, this.passUser.value, this.userform.value)
        .then((result) => {
          console.log(result);
          this.route.navigate(['setting/home']);
        });
    } else {
      alert('กรุณากรอกข้อมูลให้ครบ');
    }
  }

  Register() {
    console.log('add', this.form.value);
    this.auth
      .AddAdmin(this.email.value, this.pass.value, this.form.value)
      .then((result) => {
        console.log(result);
        this.form.reset();
        this.route.navigate(['/setting/home']);
      });
  }
}
