import { Component, OnInit, NgModule } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFireDatabase } from 'angularfire2/database';
import { Agent } from 'src/app/models/map.model';
import { SnackbarUpdateComponent } from 'src/app/dashboard/components/snackbar/snackbar-update/snackbar-update.component';
import { SnackbarComponent } from 'src/app/dashboard/components/snackbar/snackbar-save/snackbar.component';
import '@angular/compiler' ;
@Component({
  selector: 'app-suicide',
  templateUrl: './suicide.component.html',
  styleUrls: ['./suicide.component.css']
})
export class SuicideComponent implements OnInit {
  form: FormGroup;
  NewAgentForm: FormGroup;
  panelOpenState = false;
  intent: any[] = [];
  IntentFillForm;
  get agent(){
    return this.form.get('agent');
  }
  get score1(){
    return this.form.get('score1');
  }
  get score2(){
    return this.form.get('score2');
  }
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private snackbar: MatSnackBar,
    private afs: AngularFireDatabase
  ) { }

  ngOnInit(): void {
    // this.getIntent();
    this.initForm();
  }
  private initForm() {
    this.form = this.fb.group({
      agent: [ '' , [Validators.required]],
      score1: ['', [Validators.required]],
      score2: ['', [Validators.required]],
    });
    this.NewAgentForm = this.fb.group({
      agent: [ '' , [Validators.required]],
      score1: ['', [Validators.required]],
      score2: ['', [Validators.required]],
    });
  }

  // addIntent(){
  //   this.firebase.addIntent(this.form);
  //   this.form.reset();
  //   this.snackbar.openFromComponent(SnackbarComponent, { duration: 1000 });
  // }

  checkKeyIntent(item){
    this.agent.patchValue(item.value.agent);
    this.score1.patchValue(item.value.score1);
    this.score2.patchValue(item.value.score2);
  }

  // getIntent() {
  //   this.firebase.getIntent().subscribe(data => {
  //     this.intent = data.map(e => {
  //       return {
  //         key: e.key,
  //         value: e.payload.val(),
  //       }as Agent;
  //     });
  //     console.log(this.intent );
  //   });

  // }

  // editAgent(item,data){
  //   this.firebase.updateIntent(item.key, data.value);
  //   this.snackbar.openFromComponent(SnackbarUpdateComponent, { duration: 1000 });
  //   console.log(data.value);
  //   console.log(item.key);
  //   this.form.reset();
  // }

}
