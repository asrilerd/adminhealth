import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuicideComponent } from './suicide/suicide.component';


const routes: Routes = [
  { path:  'suicide' , component : SuicideComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class SettingChatbotRoutingModule { }
