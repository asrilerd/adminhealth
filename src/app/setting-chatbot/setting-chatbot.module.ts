import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingChatbotRoutingModule } from './setting-chatbot-routing.module';
import { SuicideComponent } from './suicide/suicide.component';
import { DashboardModule } from '../dashboard/dashboard.module';
import { NavbarComponent } from '../navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DialogModule } from '../dialog/dialog.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';

@NgModule({
  declarations: [SuicideComponent],
  imports: [
    CommonModule,
    SettingChatbotRoutingModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    ReactiveFormsModule,
    FormsModule,
    DialogModule,
    AngularMaterialModule,
    // AngularFireModule.initializeApp(environment.firebaseSuicide)
  ]
})
export class SettingChatbotModule { }
