export interface MapMarker {
  key?: string;
  locationName?: string ;
  latitude?: number;
  longitude?: number;
}
export interface Agent {
  key?: string;
  agent?: string ;
  score1?: number;
  score2?: number;
}
export interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}
export interface TestDetail {
  testName: string;
  currentDate: any;
  amout: number;
  averageScore: number;
  averageResult: string;
}

export interface TestList {
  user: string;
  date: any;
  gender: string;
  score: number;
  result: string;
}

