import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(
    private afs: AngularFireDatabase ,
  ) { }

  //allaboutTest
  getTestList_Happy(){
    return this.afs.list('happinessScore').snapshotChanges();
  }
  getTestList_Suicide(){
    return this.afs.list('suicideScore').snapshotChanges();
  }
  getTestList_Stress(){
    return this.afs.list('stressScore').snapshotChanges();
  }
  getTestList_Depression(){
    return this.afs.list('depressScore').snapshotChanges();
  }
  getuserRoleDoctor(uid){
    return this.afs.list('Doctor' ,ref => ref.orderByChild('uid').equalTo(uid)).valueChanges();
  }

  // getTestvalue_Depression(start?,end?){
  //   return this.afs.list('depressScore').valueChanges();
  // }

  //userDatail
  getDatailUser(username:string){
    return this.afs.list('User', ref => ref.orderByChild('user').equalTo(username) ).snapshotChanges()
  }
  getUserTest_Happy(uid){
    return this.afs.list(`happinessScore/${uid}`, ref=> ref.child('result')).snapshotChanges();
  }
  getUserTest_Suicide(uid){
    return this.afs.list(`suicideScore/${uid}`,ref=> ref.child('result')).snapshotChanges();
  }
  getUserTest_Stress(uid){
    return this.afs.list(`stressScore/${uid}` , ref=> ref.child('result')).snapshotChanges();
  }
  getUserTest_Depression(uid){
    return this.afs.list(`depressScore/${uid}` , ref=> ref.child('result')).snapshotChanges();
  }
}
