import { Injectable, NgZone } from '@angular/core';
import {
  AngularFireDatabase,
  AngularFireObject,
  AngularFireList,
} from '@angular/fire/database';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { User } from 'firebase';
import { Observable } from 'rxjs/internal/Observable';
import { ActivatedRoute } from '@angular/router';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { NgForm, FormGroup } from '@angular/forms';
import * as firebase from 'firebase';
import * as admin from 'firebase-admin';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userData: any; // Save logged in user data
  id;
  user: any;
  userList: any;
  hotel: any;
  uid: any;
  userLogged;

  constructor(
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    private db: AngularFireDatabase,
    private ActivatedRoute: ActivatedRoute,
    private dialog : MatDialog
  ) {
    /* Saving user data in localstorage when
    logged in and setting up null when logged out */
  }

  // Sign in with email/password
  SignIn(email, password) {
    return this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log('login', result);
        localStorage.setItem('user', JSON.stringify(result));
        firebase
          .database()
          .ref('User')
          .orderByChild('email')
          .equalTo(email)
          .on('child_added', (snap) => {
            console.log('ค่าทั้งหมด', snap.val());
            if (snap.val().role == 'admin') {
              this.ngZone.run(() => {
                this.router.navigate(['setting/home']);
              });
            } else if (snap.val().role == 'doctor') {
              this.ngZone.run(() => {
                this.router.navigate(['Doctorhome']);
              });
            }
          });
        this.userLogged = result;
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

  GoogleAuthDialog() {
    return this.AuthLogindialog(new auth.GoogleAuthProvider());
  }

  AuthLogindialog(provider) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {
        //  this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  //
  SignInAdmin(emailadmin, passwordAdmin) {
    return this.afAuth
      .signInWithEmailAndPassword(emailadmin, passwordAdmin)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['admindashboard']);
        });
        // this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

  // Sign up with email/password
  SignUp(email, password, data: NgForm) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.db
          .list('/User')
          .push({ email: email, password: password })
          .then((result) => {
            console.log(result);
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

  //AdduserAdmin
  AddUser(email,password,data) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.db
          .list('/User')
          .push(data);
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }
  AddAdmin(email, password, data) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.db
          .list('/User')
          .push(data);
      })
      .catch((error) => {
        window.alert(error.message);
      });
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null ? true : false;
  }

  // Sign in with Google
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }
  FacebookAuth() {
    return this.AuthLogin(new auth.FacebookAuthProvider());
  }

  // Auth logic to run auth providers
  AuthLogin(provider) {
    return this.afAuth
      .signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['home']);
        });
        //  this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  SetUserData(user) {
    const userRef: AngularFireList<any> = this.db.list(`Admin/${user.uid}`);
    const userData: any = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }

  // Sign out
  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['']);
    });
  }

  SignOutForAdmin() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['']);
    });
  }
}
