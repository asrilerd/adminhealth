import { Component, NgZone, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { auth } from 'firebase/app';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-singlepage',
  templateUrl: './singlepage.component.html',
  styleUrls: ['./singlepage.component.css'],
})
export class SinglepageComponent implements OnInit {
  myUser = JSON.parse(localStorage.getItem('user'));
  email = this.myUser?.user?.email;
  checkUserList;
  constructor(
    private dialog: MatDialog,
    private auth: AuthService,
    private router: Router,
    private afs: AngularFireDatabase,
    public ngZone: NgZone,
  ) {}

  ngOnInit(): void {

  }

  loginDialog() {
    if (this.auth.isLoggedIn !== true) {
      this.dialog.open(LoginComponent);
    } else {
      firebase
      .database()
      .ref('User')
      .orderByChild('email')
      .equalTo(this.email)
      .on('child_added', (snap) => {
        console.log('ค่าทั้งหมด', snap.val());
        if (snap.val().role == 'admin') {
          this.ngZone.run(() => {
            this.router.navigate(['setting/home']);
          });
        } else if (snap.val().role == 'doctor') {
          this.ngZone.run(() => {
            this.router.navigate(['testReport']);
          });
        }
      });
    }
  }
}
