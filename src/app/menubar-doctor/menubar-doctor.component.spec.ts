import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenubarDoctorComponent } from './menubar-doctor.component';

describe('MenubarDoctorComponent', () => {
  let component: MenubarDoctorComponent;
  let fixture: ComponentFixture<MenubarDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenubarDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenubarDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
