import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../auth/auth.service';

@Component({
  selector: 'app-menubar-doctor',
  templateUrl: './menubar-doctor.component.html',
  styleUrls: ['./menubar-doctor.component.css'],
})
export class MenubarDoctorComponent implements OnInit {
  public userLogged = JSON.parse(localStorage.getItem('user'));
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {}

  logout() {
    console.log('logout');
    this.auth.SignOut();
  }
  setting() {
    this.router.navigate([`profile/${this.userLogged.user.uid}`]);
  }

  go(page){
    if(page=="home"){
      this.router.navigate(['/Doctorhome']);
    } else if(page=="stat"){
      this.router.navigate(['/testReport']);
    }else if(page=="file"){
      this.router.navigate(['/upload']);
    }

  }
}
