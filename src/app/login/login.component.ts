import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseService } from '../dashboard/services/firebase.service';
import { AuthService } from '../auth/auth.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  get email() {
    return this.form.get('email');
  }
  get pass() {
    return this.form.get('password');
  }
  type = 'password';
  icon = 'visibility';
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private auth: AuthService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.initform();
  }
  private initform() {
    this.form = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required],
    });
  }
  login() {
    this.auth.SignIn(this.email.value, this.pass.value).then(() => {
      console.log('already login');
      this.dialog.closeAll();
    });
  }
  showPass() {
    this.type = 'text';
    this.icon = 'visibility_off';
  }

  closePass() {
    this.type = 'password';
    this.icon = 'visibility';
  }
}
