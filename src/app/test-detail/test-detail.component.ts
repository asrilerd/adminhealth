import { Component, OnInit, ViewChild } from '@angular/core';
import { DoctorService } from './../service/doctor.service';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-test-detail',
  templateUrl: './test-detail.component.html',
  styleUrls: ['./test-detail.component.css'],
})
export class TestDetailComponent implements OnInit {
  userList;
  uid;
  depressList;
  stressList;
  suicideList;
  happyList;
  displayedColumns: string[] = ['no', 'date', 'time', 'score', 'result'];
  pageSize: number[] = [5, 10, 25, 30];
  totalTable;
  dataSource;
  dataSourceStress;
  dataSourceHappy;
  dataSourceSuicide;
  depressTime = [];
  depressDate = [];
  happyTime = [];
  happyDate = [];
  suicideTime = [];
  suicideDate = [];
  stressTime = [];
  stressDate = [];
  label;
  get pageIndex() {
    return this.paginator.pageIndex * this.paginator.pageSize + 1;
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private firebase: DoctorService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.Userdetail();
    this.getuid();
  }

  showPage(page) {
    console.log(page);
    const pageSize = page.pageSize;
    const pageIndex = page.pageIndex;
    const pagePrev = page.previousPageIndex;
  }

  Userdetail() {
    const username = this.route.snapshot.paramMap.get('id');
    this.firebase.getDatailUser(username).subscribe((data) => {
      this.userList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      console.log(this.userList);
    });
  }

  getuid() {
    const username = this.route.snapshot.paramMap.get('id');
    this.firebase.getDatailUser(username).subscribe((data) => {
      data.map((e) => {
        let mapUid: any = e.payload.val();
        return (this.uid = mapUid.uid);
      });
      console.log(this.uid);
      this.getHistory_Happy();
      this.getHistory_Stress();
      this.getHistory_Suicide();
      this.getHistory_depression();
    });
  }

  getHistory_depression() {
    this.firebase.getUserTest_Depression(this.uid).subscribe((data) => {
      this.depressList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.dataSource = new MatTableDataSource(this.depressList);
      this.dataSource.paginator = this.paginator;
      console.log(this.depressList);
      data.map((item) => {
        let x: any = item.payload.val();
        let dateStr = new Date(x.completeDT).toLocaleDateString();
        let hour = new Date(x.completeDT).getHours();
        let minute = new Date(x.completeDT).getMinutes();
        if (
          minute == 0 ||
          minute == 1 ||
          minute == 2 ||
          minute == 3 ||
          minute == 4 ||
          minute == 5
        ) {
          let timeStr = hour + ':' + minute + 0;
          this.depressTime.push(timeStr);
        } else {
          let timeStr = hour + ':' + minute;
          this.depressTime.push(timeStr);
        }
        this.depressDate.push(dateStr);
      });
    });
  }

  getHistory_Happy() {
    this.firebase.getUserTest_Happy(this.uid).subscribe((data) => {
      this.happyList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.dataSourceHappy = new MatTableDataSource(this.happyList);
      this.dataSourceHappy.paginator = this.paginator;
      console.log(this.happyList);
      data.map((item) => {
        let x: any = item.payload.val();
        let dateStr = new Date(x.completeDT).toLocaleDateString();
        let hour = new Date(x.completeDT).getHours();
        let minute = new Date(x.completeDT).getMinutes();
        if (
          minute == 0 ||
          minute == 1 ||
          minute == 2 ||
          minute == 3 ||
          minute == 4 ||
          minute == 5
        ) {
          let timeStr = hour + ':' + minute + 0;
          this.happyTime.push(timeStr);
        } else {
          let timeStr = hour + ':' + minute;
          this.happyTime.push(timeStr);
        }
        this.happyDate.push(dateStr);
      });
    });
  }
  getHistory_Suicide() {
    this.firebase.getUserTest_Suicide(this.uid).subscribe((data) => {
      this.suicideList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.dataSourceSuicide = new MatTableDataSource(this.suicideList);
      this.dataSourceSuicide.paginator = this.paginator;
      console.log(this.suicideList);
      data.map((item) => {
        let x: any = item.payload.val();
        let dateStr = new Date(x.completeDT).toLocaleDateString();
        let hour = new Date(x.completeDT).getHours();
        let minute = new Date(x.completeDT).getMinutes();
        let sec = new Date(x.completeDT).getSeconds();
        if (
          minute == 0 ||
          minute == 1 ||
          minute == 2 ||
          minute == 3 ||
          minute == 4 ||
          minute == 5
        ) {
          let timeStr = hour + ':' + minute + 0;
          this.suicideTime.push(timeStr);
        } else {
          let timeStr = hour + ':' + minute + '';
          this.suicideTime.push(timeStr);
        }
        this.suicideDate.push(dateStr);
      });
    });
  }
  getHistory_Stress() {
    this.firebase.getUserTest_Stress(this.uid).subscribe((data) => {
      this.stressList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      this.dataSourceStress = new MatTableDataSource(this.stressList);
      this.dataSourceStress.paginator = this.paginator;
      console.log(this.stressList);
      data.map((item) => {
        let x: any = item.payload.val();
        let dateStr = new Date(x.completeDT).toLocaleDateString();
        let hour = new Date(x.completeDT).getHours();
        let minute = new Date(x.completeDT).getMinutes();
        if (
          minute == 0 ||
          minute == 1 ||
          minute == 2 ||
          minute == 3 ||
          minute == 4 ||
          minute == 5
        ) {
          let timeStr = hour + ':' + minute + 0;
          this.stressTime.push(timeStr);
        } else {
          let timeStr = hour + ':' + minute;
          this.stressTime.push(timeStr);
        }
        this.stressDate.push(dateStr);
      });
    });
  }
}
