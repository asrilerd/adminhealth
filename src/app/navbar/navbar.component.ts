import { Component, OnInit } from '@angular/core';
// import { auth } from 'firebase/app';
import { AuthService } from 'src/app/auth/auth.service';
import * as firebase from 'firebase';
import { MatSortModule } from '@angular/material/sort';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public userNow ;

  public userLogged = JSON.parse(localStorage.getItem('user'));
  constructor(
    public auth:AuthService,
    private router: Router,
  ) {}


  ngOnInit(): void {

  }



  logout(){
    this.auth.SignOut()
  }

  routerPath(path){
    this.router.navigate([path])
  }
  changedICon(event){
    console.log(event);

  }
}
