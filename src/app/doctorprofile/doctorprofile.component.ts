import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DoctorService } from '../service/doctor.service';

@Component({
  selector: 'app-doctorprofile',
  templateUrl: './doctorprofile.component.html',
  styleUrls: ['./doctorprofile.component.css'],
})
export class DoctorprofileComponent implements OnInit {
  constructor(
    private firebase: DoctorService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  uid = this.route.snapshot.paramMap.get('id');
  doctor
  ngOnInit(): void {
    this.getDoctor();
  }
  getDoctor() {
    this.firebase.getuserRoleDoctor(this.uid).subscribe((data) => {
        this.doctor = data
        console.log(this.doctor);

    });
  }
}
