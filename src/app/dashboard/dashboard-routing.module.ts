import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './components/news/news.component';
import { MapComponent } from './components/map/map.component';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { UserComponent } from './components/user/user.component';
import { EditNewsComponent } from './components/edit-news/edit-news.component';
import { AddAdminComponent } from './components/add-admin/add-admin.component';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  { path:  'map' , component : MapComponent },
  { path:  'newsUpdate' , component : NewsComponent },
  { path:  'editArtical/:id' , component : EditNewsComponent },
  { path:  'chatbot' , component : ChatbotComponent },
  { path:  'user' , component : UserComponent },
  { path:  'addAdmin' , component : AddAdminComponent },
  { path:  'home' , component : HomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
