import { Injectable } from '@angular/core';
// import {
//   AngularFireDatabase,
//   AngularFireList,
// } from 'angularfire2/database';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { pipe } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  locationList: AngularFireList<any>;
  constructor(private afs: AngularFireDatabase) {}

  //add//
  addLocation(data) {
    console.log('Data :', data.value);
    return this.afs.list('/Location').push(data.value);
  }
  sendEmail(data) {
    return this.afs.list('messages').push(data);
  }
  addIntent(data) {
    return this.afs.list('/suicideQuestion').push(data.value);
  }
  addNews(data) {
    return this.afs.list('News').push(data.value);
  }
  addAdmin(data) {
    return this.afs.list('Admin').push(data.value);
  }
  addUser(data) {
    return this.afs.list('User').push(data.value);
  }

  //get//

  getMap() {
    return this.afs.list('Location').snapshotChanges();
  }
  getHospital() {
    return this.afs
      .list('Location')
      .snapshotChanges();
  }
  getLocation(startKey?) {
    return this.afs
      .list('Location', (ref) =>
      ref.orderByKey().startAt(startKey).limitToFirst(3)
      )
      .snapshotChanges();
  }
  getLocation2(endKey?) {
    return this.afs
      .list('Location', (ref) =>
      ref.orderByKey().endAt(endKey).limitToFirst(3)
      )
      .snapshotChanges();
  }

  getIntent() {
    return this.afs.list('suicideQuestion').snapshotChanges();
  }
  getMynews() {
    return this.afs.list('News').snapshotChanges();
  }
  getNormalNews() {
    return this.afs.list('News',ref=>ref.orderByChild('type').equalTo('ข่าวทั่วไป')).snapshotChanges();
  }
  getSpecialNews() {
    return this.afs.list('News',ref=>ref.orderByChild('type').equalTo('ประกาศสำคัญ')).snapshotChanges();
  }
  getAdmin() {
    return this.afs
      .list('User', (ref) => ref.orderByChild('role').equalTo('admin'))
      .snapshotChanges();
  }
  getAdmin2(startKey?) {
    return this.afs
      .list('User', (ref) =>
      ref.orderByKey().startAt(startKey).limitToFirst(3)
      )
      .snapshotChanges();
  }
  getAdmin3(endKey?) {
    return this.afs
      .list('User', (ref) =>
      ref.orderByKey().endAt(endKey).limitToFirst(3)
      )
      .snapshotChanges();
  }
  totalAdmin() {
    return this.afs.list('Admin').snapshotChanges();
  }
  getUser() {
    return this.afs.list('User').snapshotChanges();
  }
  getUserbyUID(uid) {
    return this.afs.list('User', ref => ref.orderByChild("uid").equalTo(uid)).snapshotChanges();
  }

  getDoctor() {
    return this.afs
      .list('User', (ref) => ref.orderByChild('role').equalTo('doctor'))
      .snapshotChanges();
  }
  getOnlyUser() {
    return this.afs
      .list('User', (ref) => ref.orderByChild('role').equalTo('user'))
      .snapshotChanges();
  }

  getdepressScore() {
    return this.afs.list('depressScore').snapshotChanges();
  }
  gethappinessScore() {
    return this.afs.list('happinessScore').snapshotChanges();
  }
  getstressScore() {
    return this.afs.list('stressScore').snapshotChanges();
  }
  getsuicideScore() {
    return this.afs.list('suicideScore').snapshotChanges();
  }

  //update//
  updateLocation(key: string, data: any) {
    return this.afs.list('Location').update(key, data);
  }
  updateIntent(key: string, data: any) {
    return this.afs.list('suicideQuestion').update(key, data);
  }
  updateNews(key: string, data: any) {
    return this.afs.list('News').update(key, data);
  }
  uploadList(pageSize) {
    return this.afs
      .list('Document', (ref) => ref.limitToFirst(pageSize))
      .snapshotChanges();
  }
  updateNewFile(key: string, data: any) {
    return this.afs.list('Document').update(key, data);
  }
  updateAdmin(key: string, data: any) {
    return this.afs.list('Admin').update(key, data);
  }
  updateUser(key: string, data: any) {
    return this.afs
      .list('User')
      .update(key, data)
      .then((result) => {
        console.log(result);
      });
  }
  updateDoctor(key: string, data: any) {
    return this.afs.list('Doctor').update(key, data);
  }

  //delete//
  deleteNews(key: string) {
    return this.afs.list('News').remove(key);
  }
  deleteAdmin(key: string) {
    return this.afs.list('Admin').remove(key);
  }
  deleteUser(key: string) {
    return this.afs.list('User').remove(key);
  }
  deleteDoctor(key: string) {
    return this.afs.list('Doctor').remove(key);
  }
}
