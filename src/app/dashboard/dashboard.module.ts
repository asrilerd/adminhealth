import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { MapComponent } from './components/map/map.component';
import { NewsComponent } from './components/news/news.component';
import { AgmCoreModule } from '@agm/core';
import { from } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// firebase
import { AngularFireModule } from '@angular/fire';
// import { AngularFireAuthModule } from 'angularfire2/auth';
// import { AngularFireDatabaseModule } from '@angular/fire/database';
// import { AngularFireDatabaseModule } from 'angularfire2/database';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NavbarComponent } from '../navbar/navbar.component';
import { DialogModule } from '../dialog/dialog.module';
import { SnackbarComponent } from './components/snackbar/snackbar-save/snackbar.component';
import { SnackbarUpdateComponent } from './components/snackbar/snackbar-update/snackbar-update.component';
import { ShowmapComponent } from './components/showmap/showmap.component';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { UserComponent } from './components/user/user.component';

import { AngularFireStorageModule } from '@angular/fire/storage';
import { EditNewsComponent } from './components/edit-news/edit-news.component';
import { AddAdminComponent } from './components/add-admin/add-admin.component';
import { HomeComponent } from './components/home/home.component';
import { PaginatorModule } from '../paginator/paginator.module';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { NavhornzComponent } from '../navhornz/navhornz.component';
import { AdminEditProfileComponent } from './components/admin-edit-profile/admin-edit-profile.component';
import {QuillModule} from 'ngx-quill'

@NgModule({
  declarations: [
    MapComponent,
    NewsComponent,
    NavbarComponent,
    SnackbarComponent,
    SnackbarUpdateComponent,
    ShowmapComponent,
    ChatbotComponent,
    UserComponent,
    EditNewsComponent,
    AddAdminComponent,
    HomeComponent,
    NavhornzComponent,
    AdminEditProfileComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AngularMaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCqBau3CoAgOrUUux7iwl8Doz6VhcPVk04',
    }),
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    DialogModule,
    ReactiveFormsModule,
    FormsModule,
    PaginatorModule,
    QuillModule.forRoot(),
  ],
  entryComponents: [SnackbarComponent, SnackbarUpdateComponent,AdminEditProfileComponent],
})
export class DashboardModule {}
