import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseService } from '../../services/firebase.service';
import { SnackbarComponent } from '../snackbar/snackbar-save/snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarUpdateComponent } from '../snackbar/snackbar-update/snackbar-update.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { Agent } from 'src/app/models/map.model';
import { retry } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
// import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css'],
})
export class ChatbotComponent implements OnInit {
  form: FormGroup;
  $key: string ;
  file:File;
  name:string;
  url:string;
  progress:number;
  createdAt: Date = new Date()

  get formEdit(){
    return this.form ;
  }
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private snackbar: MatSnackBar,
    private afs: AngularFireDatabase,
    ) {}

  ngOnInit(): void {

    this.initForm();
  }
  private initForm() {
    this.form = this.fb.group({
      agent: [ '' ],
      score1: [''],
      score2: [''],
      choice1: ['', ],
      choice2: ['', ],
    });

  }

}
