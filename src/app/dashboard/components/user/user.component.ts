import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Directive,
} from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FirebaseService } from '../../services/firebase.service';
import { EditPasswordComponent } from 'src/app/dialog/edit-password/edit-password.component';
import { Subject } from 'rxjs';
import { takeUntil, filter, switchMap } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { get } from 'http';
import { element } from 'protractor';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { log } from 'util';
import * as admin from 'firebase-admin';
import * as firebase from 'firebase';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit, OnDestroy {
  form: FormGroup;
  dataUserArray = [];
  tableList: MatTableDataSource<any>;
  status: boolean = true;
  userStatus;
  unsubscribe$ = new Subject();
  showstatus;
  searchKey;
  displayedColumns: string[] = [
    'name',
    'email',
    'userStatus',
    'gender',
    'status',
    'enable',
    'action',
  ];
  pageSize: number[] = [5, 10, 25, 30];
  totalTable;
  user;
  doctor;
  iconStatus = 'lock_open';
  lineChart = [];
  myUser = JSON.parse(localStorage.getItem('user'));

  get emailUser() {
    return this.form.get('email');
  }
  get passUser() {
    return this.form.get('password');
  }
  get pageIndex() {
    return this.paginator.pageIndex * this.paginator.pageSize + 1;
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public auth: AuthService,
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private afs: AngularFireDatabase,
    private dialog: MatDialog,
    private el: ElementRef,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.initform();
    this.getUserList();
    this.LineChart();
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private initform() {
    this.form = this.fb.group({
      email: null,
      password: null,
    });
  }
  registorPage() {
    this.route.navigate(['registor']);
  }


  getUserList() {
    this.firebase.getDoctor().subscribe((data) => {
      this.doctor = data.map( (e) => {
        return {
          key:e.key ,
          value:e.payload.val()
        };
      })
      this.firebase.getOnlyUser().subscribe((data) => {
        this.user = data.map((e) => {
          return {
            key:e.key ,
            value:e.payload.val()
          }
        })
        let accountlist = this.doctor?.concat(this.user)
        console.log(accountlist);
        return this.tableList = new MatTableDataSource(accountlist),
        this.tableList.paginator =  this.paginator
      });
    })

  }


checkdata(data){
  console.log(data);

}

  getDoctor(){
    return this.tableList = new MatTableDataSource(this.doctor),
    this.tableList.paginator =  this.paginator
  }
  getUser(){
    return this.tableList = new MatTableDataSource(this.user),
    this.tableList.paginator =  this.paginator
  }

  changeStatus(status, element) {
    let x;
    console.log(status);
    if (status === 'close') {
      this.userStatus = false;
    } else if (status === 'open') {
      this.userStatus = true;
    }
    console.log(element);
    this.afs.list('User').update(element.key, { status: this.userStatus }); //เพิ่มการเปลี่ยนสถานะของ user
  }

  addUser() {
    this.auth
      .SignUp(this.emailUser.value, this.passUser.value, this.form.value)
      .then((result) => {
        console.log(result);
      });
  }

  login() {
    this.auth.SignIn(this.emailUser.value, this.passUser.value);
  }
  logout() {
    this.auth
      .SignOut()
      .then(() => {
        console.log('success');
      })
      .catch((err) => {
        console.log(err);
      });
  }

  delete(element) {
    console.log(element.value);
    this.firebase.deleteUser(element.key);
  }

  resetPassword(element) {
    const dialogConfig: MatDialogConfig = {
      data: element,
    };
    const dialogRef = this.dialog.open(EditPasswordComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          const checkStatus = element.value.userStatus;
          console.log('pass:', data);
          return this.firebase.updateUser(element.key, data).then(
            i=>{
              console.log(i , 'updataResult');
            }).catch( err=> { console.log(err);
            })
        })
      )
      .subscribe(() => {
        this.getUserList();
      });

    console.log('edit Password', element.value.uid);
  }

  showPage(page) {
    console.log(page);
    const pageSize = page.pageSize;
    const pageIndex = page.pageIndex;
    const pagePrev = page.previousPageIndex;
  }

  //filter table

  searchClear() {
    this.searchKey = null;
    this.getUserList();
    this.applyFilter();
  }
  applyFilter() {
    console.log(this.searchKey);
    this.tableList.data = this.tableList.data.filter((option) =>
      option.value.email.toLowerCase().includes(this.searchKey)
      // option.value.firstname.toLowerCase().includes(this.searchKey)
    );
    if (this.searchKey === '') {
      this.getUserList();
    }
  }
  LineChart() {
    this.lineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: [
          'jan',
          'feb',
          'mar',
          'april',
          'may',
          'june',
          'july',
          'aug',
          'sep',
          'nov',
          'dec',
        ], //แกน x
        datasets: [
          //หมอ
          {
            label: '',
            data: [8],
            fill: false,
            lineTension: 0,
            borderWidth: 2,
            borderColor: 'hotpink',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            steppedLine: false,
          },
          //ผู้ใช้
          {
            label: '',
            data: [6],
            fill: false,
            lineTension: 0,
            borderWidth: 2,
            borderColor: 'hotpink',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            steppedLine: false,
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'cloud',
                fontSize: 10,
                callback: function (value, index, values) {
                  return value;
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }

  note = true;
  reminder = [];
  timeRemine = [];
  time = new Date();
  public myNote = JSON.parse(localStorage.getItem('noteList'));
  reminderInput(note) {
    if (this.reminder.length == 3) {
      this.reminder.shift();
    }
    let sec: any = '';
    if (this.time.getSeconds() == 0) {
      sec = this.time.getSeconds();
    }
    let timeNote = this.time.getHours() + ':' + this.time.getMinutes() + sec;
    this.reminder.push(note.value);
    this.timeRemine.push(timeNote);
    localStorage.setItem(
      'noteList',
      JSON.stringify({ text: this.reminder, time: this.timeRemine })
    );
    console.log(this.reminder);
    //  console.log(this.myNote);
  }
  deletenote(index) {
    localStorage.removeItem(index);
  }
  noteBox() {
    this.note = !this.note;
    console.log(this.note);
  }
}
