import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { MapService } from './../../services/map.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from './../../services/firebase.service';
// import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { MapMarker } from './../../../models/map.model';
import { map, takeUntil, filter, switchMap } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppSettings } from 'src/app/app-settings';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MapDialogComponent } from 'src/app/dialog/map-dialog/map-dialog.component';
import { SnackbarUpdateComponent } from '../snackbar/snackbar-update/snackbar-update.component';
import { SnackbarComponent } from '../snackbar/snackbar-save/snackbar.component';
import { MapPopupComponent } from 'src/app/dialog/map-popup/map-popup.component';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as _ from 'lodash';
declare var google;
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
})
export class MapComponent implements OnInit, OnDestroy {
  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;
  get pageIndex() {
    return this.paginator.pageIndex * this.paginator.pageSize + 1;
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  private unsubscribe$ = new Subject();
  displayedColumns: string[] = [
    // 'no',
    'place',
    'province',
    'phone',
    'action',
  ];
  dataSource;
  map = null;
  form: FormGroup;
  locationList: AngularFireList<any>;
  lat = 13.7304755;
  lng = 100.5064551;
  latitude;
  longitude;
  locationItems;
  marker: any[] = [];
  pageSize: number[] = [5, 10, 25, 30];
  totalTable;
  icon = {
    url: './assets/pin.png',
    scaledSize: {
      width: 150,
      height: 150,
    },
  };
  statusSearchTable = false;
  statusSearchBox = true;
  searchKey: string;
  nextKey: any;
  prevKeys: any[] = [];
  key = [];
  lastKey;
  firstKey;
  subscription: any;
  get startIndex(): number {
    return this.locationItems.length;
  }
  disable = false;
  prevDisable = true;
  constructor(
    private maps: MapService,
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private afs: AngularFireDatabase,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
    this.locationList = afs.list('Location');
  }

  private initForm() {
    this.form = this.fb.group({
      hospitalName: [null, [Validators.required]],
      province: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      latitude: [null, [Validators.required]],
      longitude: [null, [Validators.required]],
    });
  }

  ngOnInit() {
    this.initForm();
    this.getLocation();
    this.MarkerLocation();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  submitLocation() {
    console.log('submit');
    this.firebase.addLocation(this.form);
    this.form.reset();
    this.snackbar.openFromComponent(SnackbarComponent, { duration: 1000 });
  }

  showPage(page) {
    console.log(page);
    const pageSize = page.pageSize;
    const pageIndex = page.pageIndex;
    const pagePrev = page.previousPageIndex;
    this.getLocation();
  }
  next() {
    const index = this.locationItems.length - 1;
    const startAt = this.locationItems[index].key;
    console.log(startAt);
    this.prevKeys = startAt;
    if (this.prevKeys == this.lastKey) {
      this.disable = true;
    } else {
      this.disable = false;
    }

    if (this.locationItems[0].key == this.firstKey) {
      this.prevDisable = false;
    } else if (this.locationItems[0].key == !this.firstKey) {
      this.prevDisable = true;
    }

    this.firebase.getLocation(startAt).subscribe((data) => {
      this.locationItems = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
          prev: e.prevKey,
        };
      });
      console.log(this.locationItems, 'marker');
    });
  }
  prev() {
    const index = this.locationItems.length - 1;
    const endKey = this.locationItems[index].key;
    this.prevKeys = endKey;
    if (this.prevKeys == this.lastKey) {
      this.disable = false;
    } else {
      this.disable = false;
    }

    this.firebase.getLocation2(endKey).subscribe((data) => {
      this.locationItems = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
          prev: e.prevKey,
        };
      });
      console.log(this.locationItems, 'marker');
    });
  }

  getLocation() {
    this.firebase.getHospital().subscribe((data) => {
      this.locationItems = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
          prev: e.prevKey,
        };
      });
      console.log(this.key, 'marker');
      console.log(this.lastKey, 'marker');
      this.dataSource = new MatTableDataSource(this.locationItems)
      this.dataSource.paginator = this.paginator
    });
  }
  MarkerLocation() {
    this.firebase.getMap().subscribe((data) => {
      this.marker = data.map((e) => {
        this.key.push(e.key);
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      // console.log(this.marker, 'marker');
    });
  }
  mapEvent(event) {
    console.log(event);
  }
  renderMarkers() {
    this.locationItems.forEach((marker) => {
      this.latitude = marker.latitude;
      this.longitude = marker.longitude;
    });
  }
  deleteLocation(Location) {
    alert('ต้องการลบข้อมูลหรือไม่');
    this.locationList.remove(Location.key);
  }
  editLocation(Location: MapMarker) {
    const dialogConfig: MatDialogConfig = {
      ...AppSettings.dialogConfig,
      data: Location,
    };
    const dialogRef = this.dialog.open(MapDialogComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          return this.firebase.updateLocation(Location.key, data);
        })
      )
      .subscribe((res) => {
        this.getLocation();
        console.log('res', res);
        this.snackbar.openFromComponent(SnackbarUpdateComponent, {
          duration: 1000,
        });
      });
    console.log('edit', Location.key);
  }

  // map //
  addmapOpen() {
    const dialogRef = this.dialog.open(MapPopupComponent);
    // dialogRef.afterClosed().subscribe(
    //   () => {
    //     this.snackbar.openFromComponent(SnackbarComponent, {
    //       duration: 1000,
    //     });
    //   }
    // )
  }

  loadMap() {
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    // create LatLng object
    const myLatLng = { lat: this.lat, lng: this.lng };
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 16,
    });
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      this.renderMarkers();
      mapEle.classList.add('show-map');
    });
    console.log('mapShow 😨');
  }

  addMarker(marker: MapMarker) {
    return new google.maps.Marker({
      lat: marker.latitude,
      lng: marker.longitude,
      title: marker.locationName,
    });
  }

  goMap(item) {
    console.log(item.value.latitude, item.value.longitude);
    this.lat = item.value.latitude;
    this.lng = item.value.longitude;
  }
  searchClear() {
    this.searchKey = null;
    this.applyFilter();
    this.getLocation();
    // this.searchBox = null
    // this.applyFilterBox()
  }
  applyFilter() {
    console.log(this.searchKey);
    this.locationItems = this.locationItems.filter((option) =>
      option.value.hospitalName.toLowerCase().includes(this.searchKey)
    );
    if (this.searchKey === '') {
      this.getLocation();
    }
  }
  // applyFilterBox() {
  //   console.log(this.searchKey);
  //   this.adminBox = this.adminBox.filter(option => option.value.email.toLowerCase().includes(this.searchKey))
  //   if(this.searchKey === ""){
  //     this.getAdminList();
  //   }
}
