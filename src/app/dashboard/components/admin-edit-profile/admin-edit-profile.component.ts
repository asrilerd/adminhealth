import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/auth.service';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-admin-edit-profile',
  templateUrl: './admin-edit-profile.component.html',
  styleUrls: ['./admin-edit-profile.component.css'],
})
export class AdminEditProfileComponent implements OnInit {
  form:FormGroup
  alertStatus=true;
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private auth: AuthService,
    private afs: AngularFireDatabase,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AdminEditProfileComponent, any>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.initform(this.data.value);
  }

  private initform(userData) {
    this.form = this.fb.group({
      firstname: [null,Validators.required] ,
      lastname : [null,Validators.required],
      username : [null,Validators.required],
      gender : [null,Validators.required]
    });
    this.form.patchValue(userData);

  }

  update(){
    if(this.form.valid){
      this.alertStatus =false;
      setTimeout( ()=>{
        this.dialogRef.close(this.form.value);
      } ,2000)
    } else {
      alert('กรุณากรอกข้อมูลให้ครบ')
    }
  }

}
