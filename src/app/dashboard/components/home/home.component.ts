import { Component, OnInit, ViewChild } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { async } from '@angular/core/testing';
import { Chart } from 'chart.js';
import { timeStamp } from 'console';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { EditPasswordComponent } from 'src/app/dialog/edit-password/edit-password.component';
import { Subject } from 'rxjs';
import { AdminEditProfileComponent } from '../admin-edit-profile/admin-edit-profile.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  userlist: any;
  totalUser;
  datasource;
  pageSize: number[] = [3];
  totalTable;
  lineChart = [];
  status = [];

  single;
  married;
  divorce;
  separate;

  bachelor;
  master;
  middleSH;
  highSH;
  diploma;
  phD;

  age1;
  age2;
  age3;
  age4;
  age5;
  x;
  y;
  students;
  offical;
  enterprise;
  employee;
  onwer;
  unemployed;
  myusername;
  searchKey;
  key;
  get pageIndex() {
    return this.paginator.pageIndex * this.paginator.pageSize + 1;
  }
  displayedColumns: string[] = [
    'position',
    'name',
    'gender',
    'birthDate',
    'maritialStatus',
    'education',
    'career',
  ];
  myUser = JSON.parse(localStorage.getItem('user')).user.uid;
  localuser = JSON.parse(localStorage.getItem('user'));
  createAt = this.localuser.user.createdAt;
  lastlogin = this.localuser.user.lastLoginAt;
  creatAtday = new Date(parseInt(this.createAt)).toLocaleDateString();
  lastloginDate = new Date(parseInt(this.lastlogin)).toLocaleDateString();
  display;
  userEdit;
  unsubscribe$ = new Subject();
  constructor(private firebase: FirebaseService, private dialog: MatDialog) {}
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    this.getUserTotal();
    this.getuserbyID();
  }

  getuserbyID() {
    this.firebase.getUserbyUID(this.myUser).subscribe((username) => {
      this.userEdit = username.map((item) => {
        this.myusername = item.payload.val();
        this.key = item.key;
        return{
          key: item.key,
          value : item.payload.val()
        }
      });
      if (this.myusername.gender == 'หญิง') {
        this.display = './assets/businuesswoman23.png';
      } else {
        this.display = './assets/businessman.png';
      }
      console.log(this.userEdit);
    });
  }
  async getUserTotal() {
    this.firebase.getOnlyUser().subscribe((data) => {
      this.userlist = data.map((item) => {
        return {
          key: item.key,
          value: item.payload.val(),
        };
      });
      // console.log(this.userlist);
      this.totalUser = this.userlist.length;
      this.datasource = new MatTableDataSource(this.userlist);
      this.datasource.paginator = this.paginator;

      //อายุ//
      this.age1 = this.userlist.filter((item) => {
        return item.value.age >= 10 && item.value.age < 19;
      });
      this.age2 = this.userlist.filter((item) => {
        return item.value.age >= 20 && item.value.age < 29;
      });
      this.age3 = this.userlist.filter((item) => {
        return item.value.age >= 30 && item.value.age < 39;
      });
      this.age4 = this.userlist.filter((item) => {
        return item.value.age >= 40 && item.value.age < 49;
      });
      this.age5 = this.userlist.filter((item) => {
        return item.value.age > 50;
      });
      // console.log('age', this.age2);

      const genderX = this.userlist.filter((item) => {
        return item.value.gender == 'หญิง';
      });
      const genderY = this.userlist.filter((item) => {
        return item.value.gender == 'ชาย';
      });
      this.x = genderX;
      this.y = genderY;
      // console.log(this.x, this.y);
      //การศึกษา//
      const eduMiddle = this.userlist.filter((item) => {
        return item.value.education == 'มัธยมต้น';
      });
      const eduHigh = this.userlist.filter((item) => {
        return item.value.education == 'มัธยมปลายหรือเทียบเท่า';
      });
      const eduDiploma = this.userlist.filter((item) => {
        return item.value.education == 'อนุปริญญาตรี';
      });
      const eduBachelor = this.userlist.filter((item) => {
        return item.value.education == 'ปริญญาตรี';
      });
      const eduMaster = this.userlist.filter((item) => {
        return item.value.education == 'ปริญญาโท';
      });
      const eduPhD = this.userlist.filter((item) => {
        return item.value.education == 'สูงกว่าปริญญาโท';
      });

      this.middleSH = eduMiddle;
      this.highSH = eduHigh;
      this.diploma = eduDiploma;
      this.bachelor = eduBachelor;
      this.master = eduMaster;
      this.phD = eduPhD;
      //อาชีพ//
      const careerStudent = this.userlist.filter((item) => {
        return item.value.career == 'นักเรียน/นักศึกษา';
      });
      const careerOfficer = this.userlist.filter((item) => {
        return item.value.career == 'ข้าราชการ';
      });
      const careerStateEnterprise = this.userlist.filter((item) => {
        return item.value.career == 'รัฐวิสาหกิจ';
      });
      const careerOwner = this.userlist.filter((item) => {
        return item.value.career == 'เจ้าของกิจการ';
      });
      const careerEmployee = this.userlist.filter((item) => {
        return item.value.career == 'ลูกจ้าง/พนักงาน';
      });
      const careerUnEmployed = this.userlist.filter((item) => {
        return item.value.career == 'ว่างงาน';
      });
      this.students = careerStudent;
      this.offical = careerOfficer;
      this.enterprise = careerStateEnterprise;
      this.onwer = careerOwner;
      this.employee = careerEmployee;
      this.unemployed = careerUnEmployed;

      //สมรส//
      const single = this.userlist.filter((item) => {
        return item.value.maritialStatus == 'โสด';
      });
      const married = this.userlist.filter((item) => {
        return item.value.maritialStatus == 'แต่งงาน';
      });
      const divorce = this.userlist.filter((item) => {
        return item.value.maritialStatus == 'หย่าร้าง';
      });
      const seperate = this.userlist.filter((item) => {
        return item.value.maritialStatus == 'แยกกันอยู่';
      });
      this.single = single;
      this.married = married;
      this.divorce = divorce;
      this.separate = seperate;
      // console.log('total', this.totalUser);

      this.LineChart();
      this.statusChart();
      this.circleAgeChart();
    });
  }

  searchClear() {
    this.searchKey = null;
    this.getUserTotal();
    this.applyFilter();
  }
  applyFilter() {
    console.log(this.searchKey);
    this.datasource.data = this.datasource.data.filter((option) =>
      option.value.firstname.toLowerCase().includes(this.searchKey)
    );
    if (this.searchKey === '') {
      this.getUserTotal();
    }
  }
  resetPassword() {
    const dialogConfig: MatDialogConfig = {
      data: this.userEdit[0],
    };
    const dialogRef = this.dialog.open(EditPasswordComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          console.log('pass:', data);
          return this.firebase
            .updateUser(this.key, data)
            .then((i) => {
              console.log(i, 'updataResult');
            })
            .catch((err) => {
              console.log(err);
            });
        })
      )
      .subscribe(() => {
        this.getuserbyID();
      });

  }
  editProfile() {
    const dialogConfig: MatDialogConfig = {
      data: this.userEdit[0],
    };
    const dialogRef = this.dialog.open(AdminEditProfileComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          console.log('profile:', data);
          return this.firebase
            .updateUser(this.key, data)
            .then((i) => {
              console.log(i, 'updataResult');
            })
            .catch((err) => {
              console.log(err);
            });
        })
      )
      .subscribe(() => {
        this.getuserbyID();
      });
  }
  showPage(page) {
    console.log(page);
    const pageSize = page.pageSize;
    const pageIndex = page.pageIndex;
    const pagePrev = page.previousPageIndex;
  }

  LineChart() {
    this.lineChart = new Chart('EduChart', {
      type: 'line',
      data: {
        labels: [
          'มัธยมต้น',
          'มัธยมปลาย',
          'อนุปริญญา',
          'ปริญญาตรี',
          'ปริญญาโท',
          'ปริญญาเอก',
        ], //แกน x
        datasets: [
          //หญิง
          {
            label: '',
            data: [
              this.middleSH.length,
              this.highSH.length,
              this.diploma.length,
              this.bachelor.length,
              this.master.length,
              this.phD.length,
            ],
            fill: false,
            lineTension: 0,
            borderWidth: 1.5,
            borderColor: 'rgba(250,166,182, 1)',
            backgroundColor: 'rgba(250,166,182, 0.2)',
            steppedLine: false,
          },
        ],
        pointStyle: 'triangle',
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'Prompt',
                fontSize: 13,
                callback: function (value, index, values) {
                  return value;
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }
  statusChart() {
    this.status = new Chart('status', {
      type: 'bar',
      data: {
        labels: ['โสด', 'แต่งงาน', 'หย่า', 'แยกกันอยู่'], //แกน x
        datasets: [
          //หญิง
          {
            label: '',
            data: [
              this.single.length,
              this.married.length,
              this.divorce.length,
              this.separate.length,
            ],
            fill: true,
            lineTension: 0,
            borderWidth: 1,
            borderColor: 'rgba(250,166,182, 1)',
            backgroundColor: 'rgba(250,166,182, 1)',
            steppedLine: false,
          },
        ],
        pointStyle: 'triangle',
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'Prompt',
                fontSize: 10,
                callback: function (value, index, values) {
                  return value;
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }
  careerChart() {
    this.lineChart = new Chart('career', {
      type: 'line',
      data: {
        labels: [
          'มัธยมต้น',
          'มัธยมปลาย',
          'อนุปริญญา',
          'ปริญญาตรี/โท',
          'ปริญญาเอก',
        ], //แกน x
        datasets: [
          //หญิง
          {
            label: '',
            data: [1, 2, 6, 8, 9],
            fill: true,
            lineTension: 0.5,
            borderWidth: 2,
            borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            steppedLine: true,
          },
        ],
        pointStyle: 'triangle',
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
        },
        legend: {
          display: false,
        },
        animation: {
          easing: 'easeOutQuint',
        },
        scales: {
          yAxes: [
            {
              ticks: {
                precision: 0,
                beginAtZero: true,
              },
              gridLines: {},
            },
          ],
          xAxes: [
            {
              ticks: {
                beginAtZero: true,
                display: true,
                max: 4,
                fontFamily: 'Prompt',
                fontSize: 10,
                callback: function (value, index, values) {
                  return value;
                },
              },
              gridLines: {
                drawOnChartArea: false,
                drawTicks: true,
              },
            },
          ],
        },
      },
    });
  }
  circleAgeChart() {
    this.lineChart = new Chart('age', {
      type: 'pie',
      data: {
        labels: ['10-19', '20-29', '30-39', '40-49', '50+'], //แกน x
        datasets: [
          {
            label: '',
            data: [
              this.age1.length,
              this.age2.length,
              this.age3.length,
              this.age4.length,
              this.age5.length,
            ],
            fill: true,
            lineTension: 0.2,
            // borderColor: "rgba(75, 192, 192, 1)",
            backgroundColor: [
              'rgb(61,101,254)',
              'rgb(253,206,91)',
              'rgb(250,166,182)',
              'rgb(217,217,255)',
              'rgb(0,238,207)',
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        // rotation: 1 * Math.PI,
        // circumference: 1 * Math.PI,
        legend: {
          display: false,
        },
        tooltip: {
          enabled: false,
        },
        cutoutPercentage: 60,
      },
      scale: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    });
  }
}
