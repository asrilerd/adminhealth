import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { FirebaseService } from '../../services/firebase.service';
// import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from 'src/app/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddnewsComponent } from 'src/app/dialog/addnews/addnews.component';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
})
export class NewsComponent implements OnInit {
  form: FormGroup;
  newsList: any[] = [];
  currentDay = new Date();
  day = this.currentDay.toDateString();
  email: string = 'asrilerd@gmail.com';
  file: any;
  newsType: string[] = ['ข่าวทั่วไป', 'ประกาศสำคัญ'];
  mailTapStatus = false;
  mailUrl =
    'https://us-central1-moodtracker-20fb0.cloudfunctions.net/sendContactTest';
  downloadURL;
  filename;
  filetype;
  imgFile;
  searchkey;
  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private authService: AuthService,
    private db: AngularFireDatabase,
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog,
    private storage: AngularFireStorage
  ) {}

  ngOnInit() {
    this.initForm();
    this.getNews();
  }

  private initForm() {
    // const day = this.currentDay.toDateString();
    this.form = this.fb.group({
      topic: [null, [Validators.required]],
      article: [null, [Validators.required]],
      createDT: [this.day, [Validators.required]],
      type: [null],
    });
  }
  mailTap() {
    this.mailTapStatus = !this.mailTapStatus;
  }

  send() {
    const { topic, article } = this.form.value;
    const nameFile = this.filename;
    const typeFile = this.filetype;
    const date = this.currentDay.toDateString();
    const url = this.downloadURL;
    const html = `
      <style>
          h1{
            display:flex;
            justify-content:center;
            align-items:center;
          }
      </style>
      <h1>MoodTracker App</h1>
      <h2>  ${topic}</h2>
      <div> ${article}</div>
    `;
    const formRequest = { topic, article, html, url, nameFile, typeFile };
    this.firebase.sendEmail(formRequest);
    // this.http.post(this.mailUrl,formRequest).subscribe()
    console.log('hey', formRequest);
    this.form.reset();
  }

  newsUpload() {
    this.firebase.addNews(this.form);
    console.log('complete Upload!!!');
    this.form.reset();
    this.getNews();
  }

  newsMode(value){
    console.log(value);
  }

  getNews() {
    this.firebase.getMynews().subscribe((data) => {
      this.newsList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      console.log(this.newsList);
    });
  }

  getNormalNews(){
    this.firebase.getNormalNews().subscribe((data) => {
      this.newsList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      console.log(this.newsList);
    });

  }
  getSpecialNews(){
    this.firebase.getSpecialNews().subscribe((data) => {
      this.newsList = data.map((e) => {
        return {
          key: e.key,
          value: e.payload.val(),
        };
      });
      console.log(this.newsList);
    });
  }

  editArtical(data) {
    console.log(data);
    this.router.navigate([`editArtical/${data.key}`]);
  }
  delete(data) {
    this.firebase.deleteNews(data.key);
  }
  uploadStatus;
  progressbar;
  onFileChanges(event) {
    var n = Date.now();
    const file = event.target.files[0];
    this.file = file;
    console.log(file.name, file.type);
    if (file.type == 'application/pdf') {
      this.imgFile  = './assets/png/pdf.png';
    } else if (file.type =='application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
      this.imgFile = './assets/png/docx.png';
    } else if(file.type  == 'image/jpeg' || file.type  == 'image/png'  ){
      this.imgFile = './assets/png/pictures.png';
    }else if(file.type  == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
      this.imgFile = './assets/png/xls.png';
    }else if(file.type  == 'application/vnd.openxmlformats-officedocument.presentationml.presentation'){
      this.imgFile = './assets/png/ppt.png';
    }
    const filePath = `attachment/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`attachment/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(async () => {
          this.downloadURL = await fileRef.getDownloadURL().toPromise();
          this.filename = file.name;
          this.filetype = file.type;
        })
      )
      .subscribe((url) => {
        if (url) {
          console.log(url.state);
          console.log(url);
          this.uploadStatus = url.state;
          this.progressbar = (url.bytesTransferred / url.totalBytes) * 100;
        }
        console.log(this.progressbar);
      });
  }

  addNews() {
    console.log('edit');
    this.dialog
      .open(AddnewsComponent)
      .afterClosed()
      .subscribe((result) => {
        this.getNews();
      });
  }
  applyFilter(){
    console.log(this.searchkey);
    this.newsList  = this.newsList.filter(option => option.value.topic.toLowerCase().includes(this.searchkey))
    if(this.searchkey === ""){
      this.getNews();
    }
  }

  searchClear() {
    this.searchkey = null;
    this.getNews();
    this.applyFilter()
  }
  next() {
    console.log('next');

  }
  prev() {
    console.log('prev');

  }
}
