import { Component, OnInit } from '@angular/core';
// import { AngularFireList } from 'angularfire2/database';
import { MapMarker } from 'src/app/models/map.model';
import { AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'app-showmap',
  templateUrl: './showmap.component.html',
  styleUrls: ['./showmap.component.css']
})
export class ShowmapComponent implements OnInit {

  locationItems: MapMarker[] = [];
  map = null;
  locationList: AngularFireList<any>;
  lat = 13.7304755 ;
  lng = 100.5064551;
  latitude;
  longitude;
  firebase: any;
  constructor() { }

  ngOnInit(): void {
  }

  getLocation() {
    this.firebase.getLocation().subscribe(data => {
      this.locationItems = data.map(e => {
        return {
          key: e.key,
          value: e.payload.val()
        } as MapMarker;
      });
      console.log(this.locationItems );
    });
  }
  mapEvent(event){
    console.log(event);
  }
}
