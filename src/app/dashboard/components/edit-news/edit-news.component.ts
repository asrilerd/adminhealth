import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { get } from 'http';
// import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NewsEditComponent } from 'src/app/dialog/news-edit/news-edit.component';
import { Subject } from 'rxjs';
import { takeUntil, filter, switchMap } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.css'],
})
export class EditNewsComponent implements OnInit , OnDestroy {
  topic: any ={};
  id = this.route.snapshot.paramMap.get('id');
  private unsubscribe$ = new Subject();
  constructor(
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private afs: AngularFireDatabase,
    private dialog: MatDialog,
    ) {}

  ngOnInit(): void {
    // const  id = this.route.snapshot.paramMap.get('id');
    this.getDatabyKey(this.id);

  }

  ngOnDestroy():void{
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getDatabyKey(id) {
    this.afs.object('News/'+id)
     .snapshotChanges()
     .subscribe((data) => {
      this.topic = data.payload.val()
      } )
      console.log(this.topic);
  }

  edit(topic){
    console.log('edit');
    const dialogConfig: MatDialogConfig = {
      data:topic
    }
    const dialogRef = this.dialog.open(NewsEditComponent, dialogConfig)
    dialogRef
    .afterClosed()
    .pipe(
         takeUntil(this.unsubscribe$),
         filter(data => !!data),
         switchMap( data => {
           return this.firebase.updateNews(this.id , data)
         })
    ).subscribe( res => {
      this.getDatabyKey(this.id)
    })

  }

}
