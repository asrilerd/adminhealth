import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FirebaseService } from 'src/app/dashboard/services/firebase.service';
import { element } from 'protractor';
import { AuthService } from 'src/app/auth/auth.service';
import { async } from '@angular/core/testing';
import { __await } from 'tslib';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { EditPasswordComponent } from 'src/app/dialog/edit-password/edit-password.component';
import { Subject } from 'rxjs';
import { takeUntil, filter, switchMap } from 'rxjs/operators';
import * as firebase from 'firebase';
import { PaginationParams } from 'src/app/models/pagination.model';
import { AppSettings } from 'src/app/app-settings';
import { AngularFireDatabase } from '@angular/fire/database';
import { threadId } from 'worker_threads';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.css'],
})
export class AddAdminComponent implements OnInit, OnDestroy {
  form: FormGroup;
  adminlist;
  status: boolean = true;
  adminStatus;
  buttonStatus = 'ปิด';
  unsubscribe$ = new Subject();
  showstatus;
  searchKey:string;
  displayedColumns: string[] = ['no','email', 'status', 'enable', 'action'];
  total: number;
  pageSize: number[] = [5, 10, 25, 30];
  totalTable;
   get emailAdmin() {
    return this.form.get('email');
  }
  page;
  get passAdmin() {
    return this.form.get('password');
  }
  get pageIndex () {
   return (this.paginator.pageIndex  *  this.paginator.pageSize )+1 ;
  }
  toggleTable = false ;
  statusSearchTable = false ;
  statusSearchBox = true ;
  adminBox;
  searchBox ;
  // disable = false;
  // prevDisable = true;
  // nextKey: any;
  // prevKeys: any[] = [];
  // key = [];
  // lastKey;
  // firstKey;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private fb: FormBuilder,
    private firebase: FirebaseService,
    private auth: AuthService,
    private afs: AngularFireDatabase,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.initform();
    this.getAdminList();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  showPage(page) {
    console.log("page",page);
    const pageSize = page.pageSize ;
    const pageIndex = page.pageIndex ;
    const pagePrev  = page.previousPageIndex ;
    this.getAdminList();
  }
  btn(event){
    console.log(event);
    document.getElementById('btnText1').style.display = "inline"
    document.getElementById('btnText1').style.color = "#335Dff"
    document.getElementById('tableBtn').style.backgroundColor = "#D7E6FF"
    document.getElementById('btnIcon1').style.color = "#335Dff"
  }
  btnleave(event){
    console.log(event);
    document.getElementById('btnText1').style.display = "none"
    document.getElementById('btnIcon1').style.color = "#D4DAF6"
    document.getElementById('tableBtn').style.backgroundColor = "transparent"


  }
  btn2(event){
    console.log(event);
    document.getElementById('btnText2').style.display = "inline"
    document.getElementById('btnText2').style.color = "#335Dff"
    document.getElementById('btnIcon2').style.color = "#335Dff"
    document.getElementById('listBtn').style.backgroundColor = "#D7E6FF"


  }
  btnleave2(event){
    console.log(event);
    document.getElementById('btnText2').style.display = "none"
    document.getElementById('btnIcon2').style.color = "#D4DAF6"
    document.getElementById('listBtn').style.backgroundColor = "transparent"


  }
  //filter table

  searchClear() {
    this.searchKey = null;
    this.getAdminList();
    this.applyFilter()
    this.searchBox = null
    this.applyFilterBox()
  }
  applyFilter() {
    console.log(this.searchKey);
    this.adminlist.data  = this.adminlist.data.filter(option => option.value.email.toLowerCase().includes(this.searchKey))
    if(this.searchKey === ""){
      this.getAdminList();
    }
  }
  applyFilterBox() {
    console.log(this.searchBox);
    this.adminBox = this.adminBox.filter(option => option.value.email.toLowerCase().includes(this.searchBox))
    if(this.searchBox === ""){
      this.getAdminList();
    }
  }

  private initform() {
    this.form = this.fb.group({
      email: null,
      password: null,
    });
  }

  getAdminList() {
    this.firebase
      .getAdmin()
      .subscribe((data) => {
        let dataArray =  data.map((e) => {
          // this.key.push(e.key);
           return {
             key: e.key ,
             value: e.payload.val()
           }
        });
        this.adminlist = new MatTableDataSource(dataArray);
        this.adminlist.paginator = this.paginator
        console.log(this.adminlist.paginator);
        this.adminBox = dataArray;
        // this.lastKey = this.key[this.key.length - 1];
        // this.firstKey = this.key[0];
      });
  }

  deleted(element) {
    console.log(element);
    this.firebase.deleteUser(element.key);
  }
  changeStatus(status , element) {
    console.log(status );
    if (status === "open") {
      this.adminStatus = true ;
    } else if ( status === "close") {
      this.adminStatus = false;
    }
    console.log(this.adminStatus);

    this.afs.list('User').update(element.key, { status: this.adminStatus });
  }

  login() {
    this.auth.SignIn(this.emailAdmin.value, this.passAdmin.value);
  }

  resetPassword(element) {
    const dialogConfig: MatDialogConfig = {
      data: element,
    };
    const dialogRef = this.dialog.open(EditPasswordComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((data) => !!data),
        switchMap((data) => {
          return this.firebase.updateUser(element.key, data);
        })
      )
      .subscribe(() => {
        this.getAdminList();
      });

    console.log('edit Password', element);
  }

  logout() {
    this.auth
      .SignOut()
      .then(() => {
        console.log('success');
      })
      .catch((err) => {
        console.log(err);
      });
  }

  tableForm(){
    this.getAdminList()
    this.toggleTable = false ;
    this.statusSearchTable = false;
    this.statusSearchBox = true;

    document.getElementById('paginatorTap').style.opacity = '0' ;
  }
  listForm(){
    this.getAdminList()
    this.toggleTable = true ;
    this.statusSearchTable = true;
    this.statusSearchBox = false;
    document.getElementById('paginatorTap').style.opacity = '1' ;
  }



}
