import { Component, OnInit } from '@angular/core';
import { DoctorService } from './../service/doctor.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { async } from '@angular/core/testing';
import * as firebase from 'firebase';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { TestDetail } from '../models/map.model';

@Component({
  selector: 'app-test-table',
  templateUrl: './test-table.component.html',
  styleUrls: ['./test-table.component.css'],
})
export class TestTableComponent implements OnInit {
  result = [];
  nameExam = [
    'แบบทดสอบโรคซึมเศร้า9Q',
    'แบบทดสอบโรคเครียด',
    'แบบทดสอบการฆ่าตัวตาย8Q',
    'แบบทดสอบตัวชี้วัดความสุข15ข้อ',
  ];
  stressList = [];
  depressLenght = 0;
  stressLenght = 0;
  happyLenght = 0;
  suicideLenght = 0;
  HappyList = [];
  suicideList = [];
  stressResult: string;
  depressResult: string;
  HappyResult: string;
  suicideResult: string;
  depressAVG: number = 0;
  suicideAVG: number = 0;
  happyAVG: number = 0;
  stressAVG: number = 0;
  depressTime;
  suicideTime;
  happyTime;
  stressTime;
  userList;
  dataLenght;
  tableStatus = false;
  range: FormGroup;
  displayedColumns: string[] = [
    'testName',
    'currentDate',
    'amout',
    'averageScore',
    'averageResult',
  ];

  get start() {
    return this.range.get('start');
  }
  get end() {
    return this.range.get('end');
  }
  data: TestDetail[];

  constructor(
    private firebase: DoctorService,
    private afs: AngularFireDatabase,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.dateForm();
  }

  private dateForm() {
    this.range = this.fb.group({
      start: null,
      end: null,
    });
  }

  table() {
    const testTable: TestDetail[] = [
      {
        testName: this.nameExam[0],
        currentDate: this.depressTime,
        amout: this.depressLenght,
        averageScore: this.depressAVG,
        averageResult: this.depressResult,
      },
      {
        testName: this.nameExam[1],
        currentDate: this.stressTime,
        amout: this.stressLenght,
        averageScore: this.stressAVG,
        averageResult: this.stressResult,
      },
      {
        testName: this.nameExam[2],
        currentDate: this.suicideTime,
        amout: this.suicideLenght,
        averageScore: this.suicideAVG,
        averageResult: this.suicideResult,
      },
      {
        testName: this.nameExam[3],
        currentDate: this.happyTime,
        amout: this.happyLenght,
        averageScore: this.happyAVG,
        averageResult: this.HappyResult,
      },
    ];
    // console.log(testTable);
    this.data = testTable;
  }

  chooseExam(exam) {
    // console.log('exam : ', exam);
    let startTimestmp = this.start.value;
    let endTimestmp = this.end.value;
    const first = startTimestmp.toDateString();
    const end = endTimestmp.toDateString();
    const firstUTC = startTimestmp.valueOf();
    const endUTC = endTimestmp.valueOf();
    this.router.navigate([`testDetailGraph/${exam}/${firstUTC}/${endUTC}`]);
  }
  search() {
    this.getDepressScore();
    this.getHappyScore();
    this.getStressScore();
    this.getSuicideScore();
    this.tableStatus = true;
  }

  getDepressScore() {
    let startTimestmp = this.start.value;
    let endTimestmp = this.end.value;
    this.depressAVG = 0;
    this.depressResult = null;
    this.depressTime = 0;
    this.firebase.getTestList_Depression().subscribe((data) => {
      data.map(async (e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('value', (snapshot) => {
            this.depressLenght = this.depressLenght + snapshot.numChildren();
            console.log(this.depressLenght);
          });
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('child_added', (snap) => {
            let length = [];
            length.push(snap.val());
            this.depressTime = new Date(
              snap.val().completeDT
            ).toLocaleDateString();
            this.depressAVG += snap.val().totalScore;
          });
      });
      setTimeout(() => {
        let x = this.depressAVG / this.depressLenght;
        this.depressAVG = x;

        if (this.depressAVG < 7) {
          this.depressResult = 'คุณไม่มีภาวะซึมเศร้า';
        } else if (this.depressAVG >= 7 && this.depressAVG <= 12) {
          this.depressResult = 'คุณมีภาวะซึมเศร้าระดับน้อย';
        } else if (this.depressAVG >= 13 && this.depressAVG <= 19) {
          this.depressResult = 'คุณมีภาวะซึมเศร้าระดับกลาง';
        } else if (this.depressAVG > 19) {
          this.depressResult = 'คุณมีภาวะซึมเศร้าระดับรุนแรง';
        }
        this.table();
        console.log(this.depressAVG);
      }, 2000);
    });
  }

  getStressScore() {
    let startTimestmp = this.start.value;
    let endTimestmp = this.end.value;
    this.stressList = [];
    this.stressTime = 0;
    this.stressAVG = 0;
    this.stressResult = null;
    this.firebase.getTestList_Stress().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('value', (snapshot) => {
            this.stressLenght = this.stressLenght + snapshot.numChildren();
            console.log(this.stressLenght);
            // this.table();
          });
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('child_added', (snap) => {
            let length = [];
            length.push(snap.val());
            this.stressTime = new Date(
              snap.val().completeDT
            ).toLocaleDateString();
            this.stressAVG += snap.val().totalScore;
            console.log('avg : ', this.stressAVG);
          });
      });
      setTimeout(() => {
        let x = this.stressAVG / this.stressLenght;
        this.stressAVG = x;
        console.log('divided', this.stressAVG);
        if (this.stressAVG >= 0 && this.stressAVG <= 4) {
          this.stressResult = 'เครียดน้อย';
        } else if (this.stressAVG >= 5 && this.stressAVG <= 7) {
          this.stressResult = 'เครียดปานกลาง';
        } else if (this.stressAVG >= 8 && this.stressAVG <= 9) {
          this.stressResult = 'เครียดมาก';
        } else if (this.stressAVG >= 10 && this.stressAVG <= 15) {
          this.stressResult = 'เครียดมากที่สุด';
        }
        this.table();
      }, 2000);
    });
  }

  getHappyScore() {
    let startTimestmp = this.start.value;
    let endTimestmp = this.end.value;
    this.HappyList = [];
    this.happyAVG = 0;
    this.happyTime = 0;
    this.HappyResult = null;
    this.firebase.getTestList_Happy().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('value', (snapshot) => {
            this.happyLenght = this.happyLenght + snapshot.numChildren();
            console.log(this.happyLenght);
          });
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('child_added', (snap) => {
            this.happyTime = new Date(
              snap.val().completeDT
            ).toLocaleDateString();
            this.happyAVG += snap.val().totalScore;
          });
      });
      setTimeout(() => {
        let x = this.happyAVG / this.happyLenght;
        this.happyAVG = x;
        console.log(this.happyAVG);
        if (this.happyAVG >= 51 && this.happyAVG <= 60) {
          this.HappyResult = 'สุขภาพจิตดีกว่าคนทั่วไป';
        } else if (this.happyAVG >= 44 && this.happyAVG <= 50) {
          this.HappyResult = 'สุขภาพจิตเท่ากับคนทั่วไป';
        } else if (this.happyAVG <= 43) {
          this.HappyResult = 'สุขภาพจิตต่ำกว่าคนทั่วไป';
        }
        this.table();
      }, 2000);
    });
  }

  getSuicideScore() {
    let startTimestmp = this.start.value;
    let endTimestmp = this.end.value;
    this.suicideAVG = 0;
    this.suicideResult = null;
    this.suicideTime = 0;
    this.firebase.getTestList_Suicide().subscribe((data) => {
      data.map((e) => {
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('value', (snapshot) => {
            this.suicideLenght = this.suicideLenght + snapshot.numChildren();
            console.log('ซูยซายเล้นท์', this.suicideLenght);
          });
        e.payload
          .child('result')
          .ref.orderByChild('completeDT')
          .startAt(startTimestmp.valueOf())
          .endAt(endTimestmp.valueOf())
          .on('child_added', (snap) => {
            // console.log('searchvalue:', snap.val());
            this.suicideTime = new Date(
              snap.val().completeDT
            ).toLocaleDateString();
            this.suicideAVG += snap.val().totalScore;
            console.log('avg : ', this.depressAVG);
          });
      });
      setTimeout(() => {
        let x = this.suicideAVG / this.suicideLenght;
        this.suicideAVG = x;
        // console.log(this.suicideAVG);
        if (this.suicideAVG === 0) {
          this.suicideResult = 'คุณไม่มีภาวะซึมเศร้า';
        } else if (this.suicideAVG >= 1 && this.suicideAVG <= 8) {
          this.suicideResult = 'แนวโน้มฆ่าตัวตายเล็กน้อย';
        } else if (this.suicideAVG >= 9 && this.suicideAVG <= 16) {
          this.suicideResult = 'แนวโน้มฆ่าตัวตายระดับปานกลาง';
        } else if (this.suicideAVG > 17) {
          this.suicideResult = 'แนวโน้มฆ่าตัวตายระดับรุนแรง';
        }
        this.table();
      }, 2000);
    });
  }

}
