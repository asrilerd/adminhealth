export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCqBau3CoAgOrUUux7iwl8Doz6VhcPVk04",
    authDomain: "moodtracker-20fb0.firebaseapp.com",
    databaseURL: "https://moodtracker-20fb0.firebaseio.com",
    projectId: "moodtracker-20fb0",
    storageBucket: "moodtracker-20fb0.appspot.com",
    messagingSenderId: "803370525269",
    appId: "1:803370525269:web:0ab762860729a2e16e2adc",
  },
};
