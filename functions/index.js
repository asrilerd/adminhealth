const functions = require('firebase-functions');
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');
const cors = require('cors')({origin:true});
const smtpTransport = require('nodemailer-smtp-transport');
const gmailEmail = encodeURIComponent(functions.config().gmail.email);
const gmailPassword = encodeURIComponent(functions.config().gmail.password);
const sgMail = require('@sendgrid/mail');
const { emit } = require('process');
const { user } = require('firebase-functions/lib/providers/auth');
const SEND_GRID_API = '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'
sgMail.setApiKey(SEND_GRID_API);

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'srilerdaun@gmail.com', // your email
    pass: 'the8lover' // your email password
  }
});
const pathUrl = "https://firebasestorage.googleapis.com/v0/b/moodtracker-20fb0.appspot.com/o/document%2F1600743264099?alt=media&token=86bb0881-2a6f-4729-98b7-6411ba8f304a"

admin.initializeApp();


exports.uidUser = functions.database.ref('/peopleInTest/depression/{pushkey}').onCreate( (snapshot, context) => {
     firebase.auth().onAuthStateChanged( useruser => {
       if (useruser){
            return admin.database().ref('depressScore').push({ uid: useruser.uid });
       } else {
          return admin.database().ref('depressScore').push({ uid: 'err' });
       }
     })


});

exports.sendContactMessage = functions.database.ref('/messages/{pushkey}').onCreate( ( snapshot , context) => {

  // const userlist = firebase.auth().currentUser;
  const val = snapshot.val() ;
  return admin.database().ref('User').on( "child_added" , (snapshot , prevChildKey) => {
      const emailval = snapshot.val() ;
      const mailOptions = {
        from: 'MoodTracker App',
        to: `${emailval.email}` ,
        subject:`${val.topic}`,
        html: `${val.html}`,
        attachments:[
          {
            filename:`${val.nameFile}`,
            path: `${val.url}`
          }
        ]
      };
      return transporter.sendMail(mailOptions, (err,info) => {
            if(err){
              console.log(err);

            }else {
              console.log( emailval );
            }
      });
  });

});
exports.sendContactTest = functions.https.onRequest((req, res) => {
  return admin.database().ref('User').on( "child_added" , (snapshot , prevChildKey) => {
      const emailval = snapshot.val() ;
      const mailOptions = {
      from: 'moodTraker App',
      to: `${emailval.email}`,
      subject: `${req.body.topic}`,
      html: `${req.body.html}`
  }
   return transporter.sendMail(mailOptions, (error, data) => {
      if (error) {
          return res.send(error.toString());
      }
      data = JSON.stringify(data)
      return res.send(`Sent! ${data}`);
      });
  });
});

exports.welcomeEmail = functions.auth.user().onCreate(user => {

  const msg = {
      to: user.email ,
      from: 'hello@fireship.io',
      subject: 'Welcome to my awesome app!',
      //custom templete
    //   templateId: 'd-da131c3565364a93966cb5cf58a872c7',
    //   dynamic_template_data: {
    //     subject: 'Welcome to my awesome app!',
    //     name: user.displayName,
    // },
  };

  return transporter.sendMail(msg)
  .then( () => console.log('email sent') )
  .catch(err => { console.log(err) } )

 });


 exports.addAdmin = functions.database.ref('/Admin/{pushkey}').onCreate( (snapshot, context) => {
       const adminlist = functions.auth.user();
       const value = snapshot.val();
       const key = snapshot.key ;
       const emailAdmin = value.email;
       const passwordAdmin = value.password
       return admin.auth().getUserByEmail(emailAdmin).then(
        adminData => {
          return admin.database().ref(`/Admin/${key}`).update({ uid: adminData.uid , status:true, userStatus:'admin'});
        });
 });



exports.adminDelete = functions.database.ref('/Admin/{pushkey}').onDelete( (snapshot, context) => {

  const value = snapshot.val()
  const key = snapshot.key ;
  const emailAdmin = value.email;
  // eslint-disable-next-line promise/always-return
  return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
      const uidAdmin = adminData.uid;
      return admin.auth().deleteUser(uidAdmin);
  });

});

exports.adminDisable = functions.database.ref('/Admin/{pushkey}').onUpdate( (change, context) => {

  const afvalue = change.after.val()
  const emailAdmin = afvalue.email;
  const status = afvalue.status;

  // eslint-disable-next-line promise/always-return
  return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
          const  uidAdmin= adminData.uid ;
          // eslint-disable-next-line promise/always-return
          admin.auth().updateUser(uidAdmin,{disabled: !status})
        //   if(status=== false ){
        //        admin.auth().updateUser(uidAdmin,{disabled: !status} )
        // } else {
        //   admin.auth().updateUser(uidAdmin,{disabled:status})
      // }
  });

});

exports.resetPassword = functions.database.ref('/Admin/{pushkey}').onUpdate( (change , context) => {
  const resetPass = change.after.val()
  const pass = resetPass.password;
  const emailAdmin = resetPass.email;
  const user = admin.auth().currentUser;
  // eslint-disable-next-line promise/always-return
  return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
    const  uidAdmin= adminData.uid ;
    admin.auth().updateUser(uidAdmin,{password:pass})
});

});

// doctor-officer //

exports.addDoctor = functions.database.ref('/Doctor/{pushkey}').onCreate( (snapshot, context) => {
  const adminlist = functions.auth.user();
  const value = snapshot.val()
  const key = snapshot.key ;
  const emailAdmin = value.email;
  const passwordAdmin = value.password
  return admin.auth().getUserByEmail(emailAdmin).then(
    adminData => {
      return admin.database().ref(`/Doctor/${key}`).update({ uid: adminData.uid , status:true , userStatus:'doctor' });
    });
});

exports.doctorDelete = functions.database.ref('/Doctor/{pushkey}').onDelete( (snapshot, context) => {

  const value = snapshot.val()
  const key = snapshot.key ;
  const emailAdmin = value.email;

  // eslint-disable-next-line promise/always-return
  return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
   const uidAdmin = adminData.uid;
   return admin.auth().deleteUser(uidAdmin);
  });

  });

  exports.doctorDisable = functions.database.ref('/Doctor/{pushkey}').onUpdate( (change, context) => {

  const afvalue = change.after.val()
  const keyData = change.after.key;
  const emailAdmin = afvalue.email;
  const status = afvalue.status;

  return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
       const uidAdmin= adminData.uid ;
       const date = new Date();
       const currentDate = date.toDateString();
       // eslint-disable-next-line promise/always-return
       if(status=== false ){
            admin.auth().updateUser(uidAdmin,{disabled:true} )
            admin.database().ref(`/Doctor/${keyData}`).update({disableDT:currentDate} )
     } else if(status=== true) {
       admin.auth().updateUser(uidAdmin,{disabled:false })
     }
  });

  });

  exports.resetPasswordDoctor = functions.database.ref('/Doctor/{pushkey}').onUpdate( (change , context) => {
    const resetPass = change.after.val()
    const pass = resetPass.password;
    const emailAdmin = resetPass.email;
    const user = admin.auth().currentUser;
    // eslint-disable-next-line promise/always-return
    return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
      const  uidAdmin= adminData.uid ;
      admin.auth().updateUser(uidAdmin,{password:pass})
  });

  });

// user //

exports.addUser = functions.database.ref('/User/{pushkey}').onCreate( (snapshot, context) => {
  const adminlist = functions.auth.user();
  const value = snapshot.val()
  const key = snapshot.key ;
  const emailAdmin = value.email;
  const passwordAdmin = value.password
  return admin.auth().getUserByEmail(emailAdmin).then(
    adminData => {
      return admin.database().ref(`/User/${key}`).update({ uid: adminData.uid , status:true});
    });
});





exports.userDelete = functions.database.ref('/User/{pushkey}').onDelete( (snapshot, context) => {

const value = snapshot.val()
const key = snapshot.key ;
const emailAdmin = value.email;

// eslint-disable-next-line promise/always-return
return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
 const uidAdmin = adminData.uid;
 return admin.auth().deleteUser(uidAdmin);
});

});

exports.userDisable = functions.database.ref('/User/{pushkey}').onUpdate( (change, context) => {

const afvalue = change.after.val()
const keyData = change.after.key;
const emailAdmin = afvalue.email;
const status = afvalue.status;

return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
     const uidAdmin= adminData.uid ;
     const date = new Date();
     const currentDate = date.toDateString();
     // eslint-disable-next-line promise/always-return
     if(status=== false ){
          admin.auth().updateUser(uidAdmin,{disabled:true} )
          admin.database().ref(`/User/${keyData}`).update({disableDT:currentDate} )
   } else if(status=== true) {
     admin.auth().updateUser(uidAdmin,{disabled:false })
   }
});

});

exports.resetPasswordUser = functions.database.ref('/User/{pushkey}').onUpdate( (change , context) => {
  const resetPass = change.after.val()
  const pass = resetPass.password;
  const emailAdmin = resetPass.email;
  const user = admin.auth().currentUser;
  // eslint-disable-next-line promise/always-return
  return admin.auth().getUserByEmail(emailAdmin).then( adminData => {
    const  uidAdmin= adminData.uid ;
    // eslint-disable-next-line promise/no-nesting
    admin.auth().updateUser(uidAdmin,{password:pass}).then(
      // eslint-disable-next-line promise/always-return
      result=>{
        return admin.database()
        .ref('User')
        .orderByChild('role')
        .equalTo('admin')
        .on( "child_added" , (snapshot , prevChildKey) => {
          const emailval = snapshot.val() ;
          const mailOptions = {
          from: 'moodTraker App',
          to: `${emailval.email}`,
          subject: 'แจ้งยืนยันการเปลี่ยนรหัสผ่าน',
          html: `<b>การเปลี่ยนรหัสผ่านของ${emailAdmin}สำเร็จ</b><br><p>รหัสผ่านใหม่ได้แก่${pass}</p>`
      }
       return transporter.sendMail(mailOptions, (error, data) => {
          if (error) {
              return res.send(error.toString());
          }
          data = JSON.stringify(data)
          return res.send(`Sent! ${data}`);
          });
      });

      }) //ส่งเมลแจ้งกรณีสำเร็จ พร้อมรหัสผ่าน บอกผู้ใช้
      .catch(err=> {console.log(err)
        return admin.database()
        .ref('User')
        .orderByChild('role')
        .equalTo('admin')
        .on( "child_added" , (snapshot , prevChildKey) => {
          const emailval = snapshot.val() ;
          const mailOptions = {
          from: 'moodTraker App',
          to: `${emailval.email}`,
          subject: 'แจ้งยืนยันการเปลี่ยนรหัสผ่าน',
          html: `<b>การเปลี่ยนรหัสผ่านของสมาชิกอีเมล${emailAdmin}ไม่สำเร็จ ระบบยังคงรหัสผ่านเก่า</p>`
      }
       return transporter.sendMail(mailOptions, (error, data) => {
          if (error) {
              return res.send(error.toString());
          }
          data = JSON.stringify(data)
          return res.send(`Sent! ${data}`);
          });
      });
    });
      // อัพเดตไฟร์เบส ให้เป็นรหัสเก่า บอกแอดมิน
});

});

// exports.iconCreate = functions.storage.bucket('icons').object( ( snapshot , context) => {

//   // const userlist = firebase.auth().currentUser;
//   // const val = snapshot.val() ;
//   // return admin.database().ref('User').on( "child_added" , (snapshot , prevChildKey) => {
//   //     const emailval = snapshot.val() ;
//   //     const mailOptions = {
//   //       from: 'MoodTracker App',
//   //       to: `${emailval.email}` ,
//   //       subject:`${val.topic}`,
//   //       html: `${val.html}`
//   //     };
//   //     return transporter.sendMail(mailOptions, (err,info) => {
//   //           if(err){
//   //             console.log(err ,emailval  );

//   //           }else {
//   //             console.log( info ,emailval );

//   //           }
//   //     });
//   });

// });
